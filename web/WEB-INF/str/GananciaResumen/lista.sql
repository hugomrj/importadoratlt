


select  gast.* , '  ' as sepa , ingre.* from  

( 

SELECT 
 
  caja_chica_gastos.cat_gasto,   categorias_gastos.descripcion g_categoria,   
  sum(caja_chica_gastos.importe) g_importe 

,ROW_NUMBER () OVER (ORDER BY caja_chica_gastos.cat_gasto) as row     
  
FROM 
  aplicacion.caja_chica_gastos, aplicacion.categorias_gastos 
WHERE 
  caja_chica_gastos.cat_gasto = categorias_gastos.cat_gasto 
  and extract(year from fecha) = v0 
  and extract(month from fecha) = v1 

  group by   caja_chica_gastos.cat_gasto, categorias_gastos.descripcion 
  
) as gast 
FULL OUTER JOIN 
(

  SELECT 
 caja_chica_ingresos.cat_ingreso, 
  categorias_ingresos.descripcion  i_categoria,   
sum(caja_chica_ingresos.importe  ) i_importe 

  ,ROW_NUMBER () OVER (ORDER BY caja_chica_ingresos.cat_ingreso) as row    
FROM 
  aplicacion.categorias_ingresos,   aplicacion.caja_chica_ingresos 
WHERE  
  caja_chica_ingresos.cat_ingreso = categorias_ingresos.cat_ingreso  
  and extract(year from fecha) = v0  
  and extract(month from fecha) = v1   

group by   caja_chica_ingresos.cat_ingreso, categorias_ingresos.descripcion  


  ) as ingre 
  ON gast.row = ingre.row  


