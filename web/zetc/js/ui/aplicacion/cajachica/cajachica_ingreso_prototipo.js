

function CajaChicaIngreso(){
    
    this.tipo = "cajachicaingreso";   
    this.recurso = "cajachicaingresos";   
    this.carpeta=  "/aplicacion";   
    
    
   this.titulosin = "Ingreso"
   this.tituloplu = "Ingresos"       
    
        
    this.campoid  = 'ingreso';
    this.tablacampos = ['fecha', 'descripcion',  'cat_ingreso.descripcion', 'importe'];        
    this.etiquetas = ['fecha', 'descripcion', 'categoria', 'importe'];        
        
   
   this.tablaformat =  ['D', 'C',  'C', 'N' ];                           
    
    this.botones_lista = "cajachicaingreso_acciones_lista";
    this.tablacamposoculto = [ ];    
    
    
    this.combobox = {"cat_ingreso":{
         "value":"cat_ingreso",
         "inner":"descripcion"}};       

    
};


CajaChicaIngreso.prototype.form_ini = function() {    
    
    var cajaingreso = document.getElementById('cajachicaingreso_importe');          
    cajaingreso.onblur  = function() {                
        cajaingreso.value  = fmtNum(cajaingreso.value);
    };     
    cajaingreso.onblur();          
    
    
};


/*
CajaIngreso.prototype.lista_new = function( obj  ) {        
    alert("lista_new");
};
*/



CajaChicaIngreso.prototype.modal_new = function(  obj  ) {    

    modal.form.new( obj );  

};





CajaChicaIngreso.prototype.tabuladores = function( cajamayor ) {  
    
};
    
    
    




CajaChicaIngreso.prototype.form_validar = function() {    

    var cajaingreso_fecha = document.getElementById('cajachicaingreso_fecha');
    if (cajaingreso_fecha.value == ""){
        msg.error.mostrar("Error en Fecha ");                    
        cajaingreso_fecha.focus();
        cajaingreso_fecha.select();                                       
        return false;        
    }
    
        
    
    var cajaingreso_importe = document.getElementById('cajachicaingreso_importe');        
    if (parseInt(NumQP(cajaingreso_importe.value)) <=  0 )         
    {
        msg.error.mostrar("Falta valor en importe");   
        cajaingreso_importe.focus();
        cajaingreso_importe.select();     
        return false;
    }  
        
    

    
    return true;
};




CajaChicaIngreso.prototype.sublista = function( obj ) {       
    
        tabla.setObjeto(obj);
        
        coleccion.lista.funciones =  [ obj.modal_new ] ;
        coleccion.lista.accion( obj );     
        
        
        //  paginacion
         document.getElementById( "cajachicaingreso_paginacion" ).innerHTML 
         = paginacion.gene();    
        
            var buscar = "";        
        //  verificacion_paginacion_move(obj, buscar, function (id) {edicion_verificacion ( id );} )

            paginacion.move(obj, buscar, function (obj, id) {modal.form.row (obj, id)}  );            
        
        
  
};









CajaChicaIngreso.prototype.carga_combos = function( obj  ) {
    

    var cajaingreso_cat_ingreso = document.getElementById("cajachicaingreso_cat_ingreso");
        var idedovalue = cajaingreso_cat_ingreso.value;
   
        ajax.url = html.url.absolute()+'/api/categoriaingreso/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();               

               
        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['cat_ingreso'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                cajaingreso_cat_ingreso.appendChild(opt);                     
            }
            
        }
            

};

