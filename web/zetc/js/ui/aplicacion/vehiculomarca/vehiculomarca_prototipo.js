

function VehiculoMarca(){
    
   this.tipo = "vehiculomarca";   
   this.recurso = "vehiculosmarcas";   
   this.value = 0;
   this.from_descrip = "descripcion";
   this.json_descrip = "descripcion";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
   
   
   this.titulosin = "Vehiculo Marca"
   this.tituloplu = "Vehiculos Marcas"   
      
   this.campoid=  'marca';
   this.tablacampos =  ['marca', 'descripcion'];
   this.etiquetas =  ['Marca', 'Descripcion'];
   
   this.tbody_id = "especialidad-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "especialidad-acciones";   
   
   this.parent = null;
   
}







VehiculoMarca.prototype.new = function( obj  ) {                

    reflex.form(obj);
    reflex.acciones.button_add(obj);          
    

    
};






VehiculoMarca.prototype.form_validar = function() {    
    return true;
};




