
function TransitoCosto(){
    
   this.tipo = "transito_costo";   
   this.recurso = "transito_costos";   
   this.value = 0;
   this.from_descrip = "";
   this.json_descrip = ['', ''];
   
   this.dom="";
   this.carpeta=  "/comercial";   
   
   this.titulosin = "Cargas en Transito";
   this.tituloplu = "Carga en Transito";      
    
   this.campoid=  'transito_costo';
   this.tablacampos =  ['cliente.nombre', 'vehiculo.modelo', 'buque_nombre',  'fecha_llegada_iqq', 'fecha_llegada_py' ];   
   
   
   
   this.etiquetas =  ['Cliente', 'Vehiculo',  'Buque',  'Llegada Iqq', 'Llegada Py' ];   
   
   this.tablaformat =  ['C', 'C', 'C', 'D', 'D' ];   
   
      
   this.tbody_id = "transito_carga-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "transito_carga-acciones";   
      
   this.tabs =  [];
   this.parent = null;
   
   
   
      /*  
   this.combobox = {"iden_doc":{
           "value":"iden_doc",
           "inner":"descripcion"}};
    */
   
}







TransitoCosto.prototype.lista_new = function( obj  ) {                
   /*
    reflex.form_new( obj );    
    reflex.acciones.button_add( obj );                     
*/
};






TransitoCosto.prototype.form_validar = function() {   
    
    return true;
};










TransitoCosto.prototype.form_ini = function() {
    
    
    var transito_costo_costo_origen = document.getElementById('transito_costo_costo_origen');          
    transito_costo_costo_origen.onblur  = function() {           
        transito_costo_costo_origen.value = fmtNum(transito_costo_costo_origen.value);                                    
    }    
    transito_costo_costo_origen.onblur();
        
    var transito_costo_flete = document.getElementById('transito_costo_flete');          
    transito_costo_flete.onblur  = function() {           
        transito_costo_flete.value = fmtNum(transito_costo_flete.value);                                    
    }    
    transito_costo_flete.onblur();
        
    var transito_costo_chapista = document.getElementById('transito_costo_chapista');          
    transito_costo_chapista.onblur  = function() {           
        transito_costo_chapista.value = fmtNum(transito_costo_chapista.value);                                    
    }    
    transito_costo_chapista.onblur();
        
        
    var transito_costo_mecanico = document.getElementById('transito_costo_mecanico');          
    transito_costo_mecanico.onblur  = function() {           
        transito_costo_mecanico.value = fmtNum(transito_costo_mecanico.value);                                    
    }    
    transito_costo_mecanico.onblur();
        
    var transito_costo_accesorios_adicionales = document.getElementById('transito_costo_accesorios_adicionales');          
    transito_costo_accesorios_adicionales.onblur  = function() {           
        transito_costo_accesorios_adicionales.value = fmtNum(transito_costo_accesorios_adicionales.value);                                    
    }    
    transito_costo_accesorios_adicionales.onblur();
        
    var transito_costo_garantias_pagadas = document.getElementById('transito_costo_garantias_pagadas');          
    transito_costo_garantias_pagadas.onblur  = function() {           
        transito_costo_garantias_pagadas.value = fmtNum(transito_costo_garantias_pagadas.value);                                    
    }    
    transito_costo_garantias_pagadas.onblur();
        
        
    var transito_costo_electricista = document.getElementById('transito_costo_electricista');          
    transito_costo_electricista.onblur  = function() {           
        transito_costo_electricista.value = fmtNum(transito_costo_electricista.value);                                    
    }    
    transito_costo_electricista.onblur();
        
        
        
        
    var transito_costo_despacho = document.getElementById('transito_costo_despacho');          
    transito_costo_despacho.onblur  = function() {           
        transito_costo_despacho.value = fmtNum(transito_costo_despacho.value);                                    
    }    
    transito_costo_despacho.onblur();

    
    
    
    var transito_costo_chapa = document.getElementById('transito_costo_chapa');          
    transito_costo_chapa.onblur  = function() {           
        transito_costo_chapa.value = fmtNum(transito_costo_chapa.value);                                    
    }    
    transito_costo_chapa.onblur();
        
    var transito_costo_comision_vendedor = document.getElementById('transito_costo_comision_vendedor');          
    transito_costo_comision_vendedor.onblur  = function() {           
        transito_costo_comision_vendedor.value = fmtNum(transito_costo_comision_vendedor.value);                                    
    }    
    transito_costo_comision_vendedor.onblur();
        
    var transito_costo_precio_venta = document.getElementById('transito_costo_precio_venta');          
    transito_costo_precio_venta.onblur  = function() {           
        transito_costo_precio_venta.value = fmtNum(transito_costo_precio_venta.value);                                    
    }    
    transito_costo_precio_venta.onblur();
        
    var transito_costo_monto_entrega = document.getElementById('transito_costo_monto_entrega');          
    transito_costo_monto_entrega.onblur  = function() {           
        transito_costo_monto_entrega.value = fmtNum(transito_costo_monto_entrega.value);                                    
    }    
    transito_costo_monto_entrega.onblur();


    
    
    var transito_costo_diferencia_guardar = document.getElementById('transito_costo_diferencia_guardar');          
    transito_costo_diferencia_guardar.onblur  = function() {           
        transito_costo_diferencia_guardar.value = fmtNum(transito_costo_diferencia_guardar.value);                                    
    }    
    transito_costo_diferencia_guardar.onblur();
    
    
    var transito_costo_nos_debe = document.getElementById('transito_costo_nos_debe');          
    transito_costo_nos_debe.onblur  = function() {           
        transito_costo_nos_debe.value = fmtNum(transito_costo_nos_debe.value);                                    
    }    
    transito_costo_nos_debe.onblur();
        
    
    var transito_costo_pago = document.getElementById('transito_costo_pago');          
    transito_costo_pago.onblur  = function() {           
        transito_costo_pago.value = fmtNum(transito_costo_pago.value);                                    
    }    
    transito_costo_pago.onblur();
        
    
    var transito_costo_costo_total = document.getElementById('transito_costo_costo_total');          
    transito_costo_costo_total.onblur  = function() {           
        transito_costo_costo_total.value = fmtNum(transito_costo_costo_total.value);                                    
    }    
    transito_costo_costo_total.onblur();

    
    var transito_costo_pago_venta_final = document.getElementById('transito_costo_pago_venta_final');          
    transito_costo_pago_venta_final.onblur  = function() {           
        transito_costo_pago_venta_final.value = fmtNum(transito_costo_pago_venta_final.value);                                    
    }    
    transito_costo_pago_venta_final.onblur();
        
    var transito_costo_comision_asesor = document.getElementById('transito_costo_comision_asesor');          
    transito_costo_comision_asesor.onblur  = function() {           
        transito_costo_comision_asesor.value = fmtNum(transito_costo_comision_asesor.value);                                    
    }    
    transito_costo_comision_asesor.onblur();
        
    var transito_costo_utilidad_bruta = document.getElementById('transito_costo_utilidad_bruta');          
    transito_costo_utilidad_bruta.onblur  = function() {           
        transito_costo_utilidad_bruta.value = fmtNum(transito_costo_utilidad_bruta.value);                                    
    }    
    transito_costo_utilidad_bruta.onblur();
        
    var transito_costo_proporcional_gasto = document.getElementById('transito_costo_proporcional_gasto');          
    transito_costo_proporcional_gasto.onblur  = function() {           
        transito_costo_proporcional_gasto.value = fmtNum(transito_costo_proporcional_gasto.value);                                    
    }    
    transito_costo_proporcional_gasto.onblur();
        
        
    var transito_costo_utilidad_neta = document.getElementById('transito_costo_utilidad_neta');          
    transito_costo_utilidad_neta.onblur  = function() {           
        transito_costo_utilidad_neta.value = fmtNum(transito_costo_utilidad_neta.value);                                    
    }    
    transito_costo_utilidad_neta.onblur();
        
    
    
};






TransitoCosto.prototype.serie = function( obj, dom, ojson ) { 
    
    
    var elementos = dom.getElementsByClassName('serie_elemento_valor');
    for(var i=0; i< elementos.length; i++) {       

        var e = elementos[i];
        
        switch(e.dataset.valor) {
            
            case "cliente_nombre":                                                 
                e.innerHTML = ojson.transito_carga.cliente.nombre ;
    
    
    
                /*
                var mos = ojson.transito_carga.cliente.nombre;
                e.onclick = function(  )
                {  
                    alert(mos);
                };       
                */
                
                
                
                break;                        

            case "iden_doc":                                  
                e.innerHTML = ojson.transito_carga.cliente.iden_doc.descripcion ;
                break;

            case "iden_numero":                                  
                e.innerHTML = ojson.transito_carga.cliente.iden_numero ;
                break;
                
            case "vehiculo":                                  
                var des = "";
                des = ojson.transito_carga.vehiculo.marca.descripcion 
                    +"&nbsp;&nbsp;&nbsp;" +ojson.transito_carga.vehiculo.modelo  
                    +"&nbsp;&nbsp;&nbsp;" +ojson.transito_carga.vehiculo.fabricacion_agno  ;
            
                e.innerHTML = des;
                break;

            case "chasis":                                  
                e.innerHTML = ojson.transito_carga.vehiculo.chasis_numero ;
                break;            

            case "costo_origen":                                  
                e.innerHTML = fmtNum(ojson.costo_origen);       
                break;            

            case "flete":                                  
                e.innerHTML = fmtNum(ojson.flete);       
                break;            



            case "chapista":                                  
                e.innerHTML = fmtNum(ojson.chapista);       
                break;            

            case "mecanico":                                  
                e.innerHTML = fmtNum(ojson.mecanico);       
                break;            

            case "accesorios_adicionales":                                  
                e.innerHTML = fmtNum(ojson.accesorios_adicionales);       
                break;            

            case "garantias_pagadas":                                  
                e.innerHTML = fmtNum(ojson.garantias_pagadas);       
                break;            

            case "electricista":                                  
                e.innerHTML = fmtNum(ojson.electricista);       
                break;            


            case "despacho":                                  
                e.innerHTML = fmtNum(ojson.despacho);       
                break;            

            case "chapa":                                  
                e.innerHTML = fmtNum(ojson.chapa);       
                break;            

            case "comision_vendedor":                                  
                e.innerHTML = fmtNum(ojson.comision_vendedor);       
                break;            

            case "precio_venta":                                  
                e.innerHTML = fmtNum(ojson.precio_venta);       
                break;            

            case "monto_entrega":                                  
                e.innerHTML = fmtNum(ojson.monto_entrega);       
                break;            

            case "diferencia_guardar":                                  
                e.innerHTML = fmtNum(ojson.diferencia_guardar);       
                break;            

            case "nos_debe":                                  
                e.innerHTML = fmtNum(ojson.nos_debe);       
                break;            

            case "pago":                                  
                e.innerHTML = fmtNum(ojson.pago);       
                break;            

            case "costo_total":                                  
                e.innerHTML = fmtNum(ojson.costo_total);       
                break;            

            case "pago_venta_final":                                  
                e.innerHTML = fmtNum(ojson.pago_venta_final);       
                break;            

            case "comision_asesor":                                  
                e.innerHTML = fmtNum(ojson.comision_asesor);       
                break;            

            case "utilidad_bruta":                                  
                e.innerHTML = fmtNum(ojson.utilidad_bruta);       
                break;            

            case "utilidad_neta":                                  
                e.innerHTML = fmtNum(ojson.utilidad_neta);       
                break;            


            case "entregado":                                  
                
//                var cam = ojson.transito_carga.entregado;       
                var cam = "";       
                
                
                if ( ojson.transito_carga.entregado.toString() == 'false' ){
                    cam = "No";
                }                
                if ( ojson.transito_carga.entregado.toString() == 'true' ){
                    cam = "Si";
                }
                
                e.innerHTML = cam;
                
                break;            




            default:
                //code block
        }                              

            
        
        //campos[i].innerHTML = "fasdfasjfkasjfkasj";


    }

    
    
    
    
    
    //var ele = dom.getElementsByTagName("div");        
    //console.log(ele.length);
    
    
    
    
};







TransitoCosto.prototype.form_id = function( obj, id ) { 

    
        ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/form.html';        
        ajax.metodo = "GET";            
    

        document.getElementById( obj.dom ).innerHTML =  ajax.public.html();    
    
    
        ajax.url = html.url.absolute()+'/api/' + obj.recurso + '/'+id;    
        ajax.metodo = "GET";

        form.name = "form_" + obj.tipo;
        form.json = ajax.private.json();   
        
        var ojson = JSON.parse(form.json) ;        
        
        
        try {            
            
            
            document.getElementById('transito_costo_transito_costo').value 
                    =  ojson["transito_costo"];          
            
            document.getElementById('transito_costo_transito_carga').value 
                    =  ojson["transito_carga"]["transito_carga"];          
            
            
            document.getElementById('carga_cliente_nombre').value 
                    =  ojson["transito_carga"]["cliente"]["nombre"] ;                        

            document.getElementById('carga_iden_doc').innerHTML 
                    =  ojson["transito_carga"]["cliente"]["iden_doc"]["descripcion"] ;                        

            document.getElementById('carga_iden_numero').value 
                    =  ojson["transito_carga"]["cliente"]["iden_numero"] ;                        

            // vehiculos
            document.getElementById('carga_marca').value 
                    =  ojson["transito_carga"]["vehiculo"]["marca"]["descripcion"] ;                                    
            
            document.getElementById('carga_modelo').value 
                    =  ojson["transito_carga"]["vehiculo"]["modelo"] ;                                    
            
            document.getElementById('carga_agno').value 
                    =  ojson["transito_carga"]["vehiculo"]["fabricacion_agno"] ;                                    
            

            // costos
            
            document.getElementById('transito_costo_costo_origen').value = fmtNum(ojson["costo_origen"]);                                    
            document.getElementById('transito_costo_flete').value = fmtNum(ojson["flete"]);                                    
            
            document.getElementById('transito_costo_chapista').value = fmtNum(ojson["chapista"]);   
            document.getElementById('transito_costo_mecanico').value = fmtNum(ojson["mecanico"]);   
            document.getElementById('transito_costo_accesorios_adicionales').value = fmtNum(ojson["accesorios_adicionales"]);   
            document.getElementById('transito_costo_garantias_pagadas').value = fmtNum(ojson["garantias_pagadas"]);   
            document.getElementById('transito_costo_electricista').value = fmtNum(ojson["electricista"]);   
            
            
            
            document.getElementById('transito_costo_despacho').value = fmtNum(ojson["despacho"]);   
            
            
            
            document.getElementById('transito_costo_chapa').value = fmtNum(ojson["chapa"]);                         
            document.getElementById('transito_costo_comision_vendedor').value = fmtNum(ojson["comision_vendedor"]);                                    
            document.getElementById('transito_costo_precio_venta').value = fmtNum(ojson["precio_venta"]);                                    
            document.getElementById('transito_costo_monto_entrega').value = fmtNum(ojson["monto_entrega"]);   
            
            document.getElementById('transito_costo_diferencia_guardar').value = fmtNum(ojson["diferencia_guardar"]);                                    
            document.getElementById('transito_costo_nos_debe').value = fmtNum(ojson["nos_debe"]);                                    
            document.getElementById('transito_costo_pago').value = fmtNum(ojson["pago"]);                                    
            document.getElementById('transito_costo_costo_total').value = fmtNum(ojson["costo_total"]);                                    
            
            document.getElementById('transito_costo_pago_venta_final').value = fmtNum(ojson["pago_venta_final"]);  
            document.getElementById('transito_costo_comision_asesor').value = fmtNum(ojson["comision_asesor"]);                                    
            document.getElementById('transito_costo_utilidad_bruta').value = fmtNum(ojson["utilidad_bruta"]);                                    
            document.getElementById('transito_costo_proporcional_gasto').value = fmtNum(ojson["proporcional_gasto"]);                                    
            
            document.getElementById('transito_costo_utilidad_neta').value = fmtNum(ojson["utilidad_neta"]);                                    

              
        }
        catch (e) {


console.log(e);


            document.getElementById('carga_cliente_nombre').value  = "";                   
            document.getElementById('carga_iden_doc').innerHTML = "";
            document.getElementById('carga_iden_numero').value = "";                      
            document.getElementById('carga_marca').value  = "";
            document.getElementById('carga_modelo').value = "";
            document.getElementById('carga_agno').value = "";
            
 
            
            document.getElementById('transito_costo_costo_origen').value = "0";                                    
            document.getElementById('transito_costo_flete').value = "0";                                    
            document.getElementById('transito_costo_chapista').value = "0";        
            document.getElementById('transito_costo_mecanico').value = "0";        
            document.getElementById('transito_costo_accesorios_adicionales').value = "0";        
            document.getElementById('transito_costo_garantias_pagadas').value = "0";        
            document.getElementById('transito_costo_electricista').value = "0";        
            
            
            document.getElementById('transito_costo_despacho').value = "0";                                       
            
            document.getElementById('transito_costo_chapa').value = "0";                                    
            document.getElementById('transito_costo_comision_vendedor').value = "0";                                                                        
            document.getElementById('transito_costo_precio_venta').value = "0";                                    
            document.getElementById('transito_costo_monto_entrega').value = "0";                                    
            
            document.getElementById('transito_costo_diferencia_guardar').value = "0";                                                                        
            document.getElementById('transito_costo_nos_debe').value = "0";                                                                        
            document.getElementById('transito_costo_pago').value = "0";                                                                        
            document.getElementById('transito_costo_costo_total').value = "0";                                                              
            
            document.getElementById('transito_costo_pago_venta_final').value = "0";                                    
            document.getElementById('transito_costo_comision_asesor').value = "0";                                    
            document.getElementById('transito_costo_utilidad_bruta').value = "0";                                                                        
            document.getElementById('transito_costo_proporcional_gasto').value = "0";                                                                        
            
            document.getElementById('transito_costo_utilidad_neta').value = "0";                                                                        
           
            
        }           


        obj.form_form_id_botones(obj);

        
};







TransitoCosto.prototype.form_form_id_botones = function( obj ) { 
    
    
        
        
        form.name = "form_transito_costo";
        form.disabled(false);
        

        boton.ini(obj);
        boton.blabels = ["Modificar", "Lista"]
        var strhtml =  boton.get_botton_base();
        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;



        var btn_transito_costo_modificar = document.getElementById('btn_transito_costo_modificar');              
        btn_transito_costo_modificar.onclick = function(  )
        {  
            var obj = new TransitoCosto();    
            obj.form_editar(obj);
        };       


        var btn_transito_costo_lista = document.getElementById('btn_transito_costo_lista');              
        btn_transito_costo_lista.onclick = function(  )
        {  
            var obj = new TransitoCosto();    
            obj.dom = 'arti_form';
            transito_costo_serie_inicio(obj);
        };       



}







TransitoCosto.prototype.form_editar = function( obj ) { 

        form.name = "form_transito_costo";
        //form.disabled(true);
        
        obj.form_ini();
        
        
        form.disabled(false);
        document.getElementById('transito_costo_pago_venta_final').disabled =  false;
        document.getElementById('transito_costo_comision_asesor').disabled =  false;
        document.getElementById('transito_costo_utilidad_bruta').disabled =  false;
        document.getElementById('transito_costo_proporcional_gasto').disabled =  false;
        document.getElementById('transito_costo_utilidad_neta').disabled =  false;
        
        
        
        boton.ini(obj);
        boton.blabels = ['Guardar', "Cancelar"]
        var strhtml =  boton.get_botton_base();
        document.getElementById(  obj.tipo + '-acciones' ).innerHTML = strhtml;        
        
        
       
        var btn_transito_costo_guardar = document.getElementById('btn_transito_costo_guardar');              
        btn_transito_costo_guardar.onclick = function(  )
        {  
                        
            
            var obj = new TransitoCosto();              

            if ( obj.form_validar())
            {
                
                var idvalue = document.getElementById('transito_costo_transito_costo');                     
                
                ajax.metodo = "put";
                ajax.url = html.url.absolute()+"/api/"+ obj.recurso+"/"+idvalue.value;               

                form.name = "form_"+obj.tipo;
                var data = ajax.private.json( form.datos.getjson() );  

                //var data = form.datos.getjson() ;

                switch (ajax.state) {

                  case 200:

                        msg.ok.mostrar("registro agregado");          
                        reflex.data  = data;
                        //reflex.form_id( obj, reflex.getJSONdataID( obj.campoid ) ) ;       


                        obj.dom = 'arti_form';
                        obj.form_id( obj, idvalue.value );
                        
                        
                        break; 


                  case 401:

                        window.location = html.url.absolute();         
                        break;                             


                  case 500:

                        msg.error.mostrar( data );          
                        break; 


                  case 502:                              
                        msg.error.mostrar( data );          
                        break;                                 


                  default: 
                    msg.error.mostrar("error de acceso");           
                }   
                
                
            }



            
        };       
        
        
        
        
        
        
        
        

        var btn_transito_costo_cancelar = document.getElementById('btn_transito_costo_cancelar');              
        btn_transito_costo_cancelar.onclick = function(  )
        {  
            obj.form_form_id_botones(obj);
        };       




        
};























