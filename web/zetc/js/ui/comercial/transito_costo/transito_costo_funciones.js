
   
   
function transito_costo_serie_inicio  ( obj ){        
                        
        busqueda.custom( obj );          
        
        
        paginacion.pagina = 1;    
            
        reflex.idbusqueda = busqueda.idtexto;
        boton.objeto = obj.nombre;                   
        
        transito_costo_serie(obj, paginacion.pagina);        
}
   




function transito_costo_serie  ( obj, page ){    
    
    
        if (!(obj.alias === undefined)) 
        {    
            ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.alias + '/htmf/serie.html';    
        }              
        else
        {
            ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/serie.html';    
        }

        
        ajax.metodo = "GET";            
        document.getElementById( obj.dom ).innerHTML =  ajax.public.html();
        reflex.getTituloplu(obj);
        
        
        var bu = "";
        bu = busqueda.getTextoBusqueda();                                                
        ajax.url = reflex.getApi( obj, page, bu );




        ajax.metodo = "GET";        
        reflex.json = ajax.private.json();    



        serie.ini(obj);
        serie.gene();
    
        transito_costo_lista_registro(obj, obj.form_id);

        
        //table.json = reflex.json;
        document.getElementById( obj.tipo + "_paginacion" ).innerHTML = paginacion.gene();     
        
        //var buscar = busqueda.getTextoBusqueda();
            
        var buscar = "";        
        transito_costo_paginacion ( obj, buscar );    
        
}
  
  
  
  
  
  
  function transito_costo_lista_registro ( obj ,fn )  {          
            
        var ulineas =  document.getElementById( "transito_costo_serie_body" ).getElementsByClassName('serie_line');

        for (var i=0 ; i < ulineas.length; i++)
        {
            ulineas[i].onclick = function()
            {                  
                var linea_id = this.dataset.linea_id;                                    
                fn( obj, linea_id );                    
            };       
        }        
        
    }
    
  
  
  
  
function transito_costo_paginacion ( obj, buscar )  {
        

        var listaUL = document.getElementById( obj.tipo + "_paginacion" );
        var uelLI = listaUL.getElementsByTagName('li');
        
        for (var i=0 ; i < uelLI.length; i++)
        {
            var datapag = uelLI[i].dataset.pagina;     

            if (!(datapag == "act"  || datapag == "det"  ))
            {
                uelLI[i].addEventListener ( 'click',
                    function() {                                      
                        
                        switch (this.dataset.pagina)
                        {
                           case "sig": 
                                   paginacion.pagina = parseInt(paginacion.pagina) +1;
                                   break;                                                                          

                           case "ant":                                     
                                   paginacion.pagina = parseInt(paginacion.pagina) -1;
                                   break;

                           default:  
                                   paginacion.pagina = this.childNodes[0].innerHTML.toString().trim();
                                   break;
                        }                 
                        paginacion.pagina = parseInt( paginacion.pagina , 10);
                        
                        transito_costo_serie  ( obj, paginacion.pagina );
                            
                    },
                    false
                );                
            }            
        }           
    
    
}      