
function TransitoCarga(){
    
   this.tipo = "transito_carga";   
   this.recurso = "transito_cargas";   
   this.value = 0;
   this.from_descrip = "";
   this.json_descrip = ['', ''];
   
   this.dom="";
   this.carpeta=  "/comercial";   
   
   this.titulosin = "Cargas en Transito";
   this.tituloplu = "";      
    
   this.campoid=  'transito_carga';
   this.tablacampos =  ['cliente.nombre', 'vehiculo.modelo', 'buque_nombre',  'fecha_salida',  'fecha_llegada_iqq', 'fecha_llegada_py' ];   
   
   
   
   this.etiquetas =  ['Cliente', 'Vehiculo',  'Buque',  'Salida',  'Llegada Iqq', 'Llegada Py' ];   
   
   this.tablaformat =  ['C', 'C', 'C', 'D', 'D' , 'D'];   
   
      
   this.tbody_id = "transito_carga-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "transito_carga-acciones";   
      
   this.tabs =  [];
   this.parent = null;
   
   
   
      /*  
   this.combobox = {"iden_doc":{
           "value":"iden_doc",
           "inner":"descripcion"}};
    */
   
}





TransitoCarga.prototype.lista_new = function( obj  ) {                
   
    reflex.form_new( obj );    
    reflex.acciones.button_add( obj );                     

};




TransitoCarga.prototype.post_lista = function( obj  ) {                
    
    var en_tansito = document.getElementById('en_tansito');    
    en_tansito.value = 0;
    
    var entregados = document.getElementById('entregados');    
    entregados.value = 0;
    
    var totalvehiculos = document.getElementById('totalvehiculos');    
    totalvehiculos.value = 0;
    
        ajax.url = html.url.absolute()+'/api/transito_cargas/consulta/totalvehiculos/' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();       
            
console.log(datajson );       

        var ojson = JSON.parse(datajson) ;   
        
        for(x=0; x < ojson.length; x++) {     
        
            if ( ojson[x].ord == 1 ){
                en_tansito.value = ojson[x].cantidad;
                en_tansito.value = fmtNum(en_tansito.value );    
                
            }
            
            if ( ojson[x].ord == 2 ){
                entregados.value = ojson[x].cantidad;
                entregados.value =  fmtNum(entregados.value );    
            }
            
            if ( ojson[x].ord == 3 ){
                totalvehiculos.value = ojson[x].cantidad;
                totalvehiculos.value = fmtNum(totalvehiculos.value );    
            }
        
        }
        


    

};













TransitoCarga.prototype.form_new = function( obj  ) {                
    
        var line_entrega = document.getElementById('line_entrega');        
        line_entrega.style.display = "none";
        
};






TransitoCarga.prototype.form_validar = function() {   
    
    
    var transito_carga_cliente = document.getElementById('transito_carga_cliente');        
    if (parseInt(NumQP(transito_carga_cliente.value)) <= 0 )         
    {
        msg.error.mostrar("Falta Cliente");    
        return false;
    }        
    
    
    
    var transito_carga_vehiculo = document.getElementById('transito_carga_vehiculo');        
    if (parseInt(NumQP(transito_carga_vehiculo.value)) <= 0 )         
    {
        msg.error.mostrar("Falata Vehiculo");    
        
        transito_carga_vehiculo.focus();
        transito_carga_vehiculo.select(); 
        return false;
    }         
    

    var transito_carga_fecha_transacion = document.getElementById('transito_carga_fecha_transacion');
     if (transito_carga_fecha_transacion.value == ""){
         msg.error.mostrar("La fecha de transaccion  no puede estar vacia ");                    
         transito_carga_fecha_transacion.focus();
         transito_carga_fecha_transacion.select();                                       
         return false;        
     } 
    
    
    
    
    var line_entrega = document.getElementById('line_entrega');
    if( line_entrega.style.display !== 'none'  ){
    
            var entregado_true = document.getElementById('entregado_true');      
            if (entregado_true.checked) {
                
                    var transito_carga_fecha_entrega = document.getElementById('transito_carga_fecha_entrega');
                    if (transito_carga_fecha_entrega.value == ""){
                        msg.error.mostrar("La fecha de entrega no puede estar vacia ");                    
                        transito_carga_fecha_entrega.focus();
                        transito_carga_fecha_entrega.select();                                       
                        return false;        
                    }

            }
            
    }
    
    
    
    return true;
};










TransitoCarga.prototype.form_ini = function() {    
    
    var transito_carga_cliente_nombre = document.getElementById('transito_carga_cliente_nombre');
    transito_carga_cliente_nombre.disabled = true;
    

    
    
    
    
    var transito_carga_cliente_iden_doc = document.getElementById('transito_carga_cliente_iden_doc');
    transito_carga_cliente_iden_doc.onchange = function() {
                
                
        if (transito_carga_cliente_iden_doc.value.toString().trim() == "C"){            
            transito_carga_cliente_iden_digito.type = 'hidden';
    //        transito_carga_cliente_iden_digito.value  = '0';
        }
        else{
            if (transito_carga_cliente_iden_doc.value.toString().trim() == "R"){            
                transito_carga_cliente_iden_digito.type = 'text';
            }
        }
    
        document.getElementById('transito_carga_cliente_iden_digito').value = '0'; 
        document.getElementById('transito_carga_cliente').value = '0'; 
        document.getElementById('transito_carga_cliente_iden_numero').value = '0'; 
        document.getElementById('transito_carga_cliente_nombre').value = ''; 
        
    };     
    transito_carga_cliente_iden_doc.onchange();

    
/*
    var transito_carga_cliente_iden_numero = document.getElementById('transito_carga_cliente_iden_numero');        
    transito_carga_cliente_iden_numero.onkeypress = function(evt) {            
        return isNumberKey(evt);
    };      
*/
    
    


    var transito_carga_cliente = document.getElementById('transito_carga_cliente');          
    transito_carga_cliente.onblur  = function() {                
        
        var  cliente_id = NumQP(fmtNum(transito_carga_cliente.value))
        
        ajax.url = html.url.absolute()+'/api/clientes/'+cliente_id ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();       
        
        if (ajax.state == 200)
        {            
            var oJson = JSON.parse(datajson) ;                
            
            transito_carga_cliente_nombre.value =  oJson['nombre']  ; 
            transito_carga_cliente_iden_numero.value =  oJson['iden_numero']  ; 
            transito_carga_cliente_iden_digito.value =  oJson['iden_digito']  ; 
            
                 
                 var transito_carga_cliente_iden_doc = document.getElementById( "transito_carga_cliente_iden_doc" );
                 var obj = new TransitoCarga();
                 obj.borrar_combo_cliente(obj);
                 

                    var opt = document.createElement('option');            
                    opt.value = oJson["iden_doc"]["iden_doc"];
                    opt.innerHTML = oJson["iden_doc"]["descripcion"];
                    transito_carga_cliente_iden_doc.appendChild(opt);           
                    
                    obj.carga_combos(obj);
            
            
        }
        else
        {
            transito_carga_cliente_nombre.value =  ""  ; 
            transito_carga_cliente_iden_numero.value =  "0" ; 
            transito_carga_cliente_iden_digito.value =  "0"  ; 
            
            var obj = new TransitoCarga();    
            obj.borrar_combo_cliente(obj);
            obj.carga_combos(obj);            
                       
            
        }
        
               
        
//console.log(datajson);        
        
        
    };     
    //transito_carga_cliente.onblur();          





    var transito_carga_cliente_iden_numero = document.getElementById('transito_carga_cliente_iden_numero');          
    transito_carga_cliente_iden_numero.onblur  = function() {                
        
        var ant = transito_carga_cliente_iden_numero.value;
        
        var obj = new TransitoCarga();
        obj.usuario_change(obj);
    
        transito_carga_cliente_iden_numero.value = ant;
        
    };     
    

    
    
    var transito_carga_cliente_iden_digito = document.getElementById('transito_carga_cliente_iden_digito');   
    transito_carga_cliente_iden_digito.onblur  = function() {                
        
        var ant = transito_carga_cliente_iden_digito.value;        
        var obj = new TransitoCarga();
        obj.usuario_change(obj);    
        transito_carga_cliente_iden_digito.value = ant;
        
    };       
    
        
    
    
    
    
    //transito_carga_cliente_iden_numero.onblur();          
    
    
    
    
    // busqueda more
    var ico_more_transito_carga_cliente = document.getElementById('ico-more-transito_carga_cliente');              
    ico_more_transito_carga_cliente.onclick = function(  )
    {  
            var obj = new Cliente();                 
            obj.acctionresul = function(id) {    
                transito_carga_cliente.value = id; 
                transito_carga_cliente.onblur(); 
            };             
            modal.ancho = 900;
            busqueda.modal.objeto(obj);
    };       


    var ico_plus_transito_carga_cliente = document.getElementById("ico-plus-transito_carga_cliente");                                
    ico_plus_transito_carga_cliente.onclick = function(  )
    {  
        
        var cliente = new Cliente();                  
        cliente.post_add_modal = function(cliente) {    

            document.getElementById("transito_carga_cliente").value = 
                    reflex.getJSONdataID( "cliente" ) ;
            document.getElementById("transito_carga_cliente").onblur();     
                
        };           
        
        modal.form.plus(cliente);
        cliente.form_ini();
        cliente.carga_combos(cliente);
        
    };                   
    
    
    var transito_carga_marca = document.getElementById("transito_carga_marca");      
    transito_carga_marca.disabled = true;

    var transito_carga_modelo = document.getElementById("transito_carga_modelo");      
    transito_carga_modelo.disabled = true;
    
    var transito_carga_agno = document.getElementById("transito_carga_agno");      
    transito_carga_agno.disabled = true;
    
    var transito_carga_chasis = document.getElementById("transito_carga_chasis");      
    transito_carga_chasis.disabled = true;
    
    
    
    
    var transito_carga_vehiculo = document.getElementById('transito_carga_vehiculo');          
    transito_carga_vehiculo.onblur  = function() {   
        
        
        transito_carga_vehiculo.value = fmtNum(transito_carga_vehiculo.value);        
        transito_carga_vehiculo.value = NumQP((transito_carga_vehiculo.value));        
        var  vehiculo_id = (transito_carga_vehiculo.value);
        
        ajax.url = html.url.absolute()+'/api/vehiculos/'+vehiculo_id ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();      


        if (ajax.state == 200)
        {            
            var oJson = JSON.parse(datajson) ;      
            
            transito_carga_marca.value =  oJson['marca']['descripcion']  ; 
            transito_carga_modelo.value =  oJson['modelo'] ; 
            transito_carga_agno.value =  oJson['fabricacion_agno'] ; 
            transito_carga_chasis.value =  oJson['chasis_numero'] ; 
            
            
        }
        else{
            
            transito_carga_marca.value =  ""; 
            transito_carga_modelo.value =  "" ; 
            transito_carga_agno.value =  "" ;             
            transito_carga_chasis.value =  ""; 
        }   
    
    }    
    transito_carga_vehiculo.onblur();
    
    
    // busqueda more
    var ico_more_transito_carga_vehiculo = document.getElementById('ico-more-transito_carga_vehiculo');              
    ico_more_transito_carga_vehiculo.onclick = function(  )
    {  
            var obj = new Vehiculo();                 
            
            
            obj.acctionresul = function(id) {    
                transito_carga_vehiculo.value = id; 
                transito_carga_vehiculo.onblur(); 
            };             
            
            modal.ancho = 900;
            busqueda.modal.objeto(obj);
    };       
    
    

    var ico_plus_transito_carga_vehiculo = document.getElementById("ico-plus-transito_carga_vehiculo");                                
    ico_plus_transito_carga_vehiculo.onclick = function(  )
    {  
        
        var vehiculo = new Vehiculo();                  
        vehiculo.post_add_modal = function(cliente) {    

            document.getElementById("transito_carga_vehiculo").value = 
                    reflex.getJSONdataID( "vehiculo" ) ;
            document.getElementById("transito_carga_vehiculo").onblur();     
                
        };           
        
        modal.form.plus(vehiculo);
        vehiculo.form_ini();
        vehiculo.carga_combos(vehiculo);
        
    };                   
        





};





TransitoCarga.prototype.carga_combos = function( obj  ) {                
    
    // cargar combo
   
        var transito_carga_cliente_iden_doc = document.getElementById("transito_carga_cliente_iden_doc");
        var idedovalue = transito_carga_cliente_iden_doc.value;
   
        ajax.url = html.url.absolute()+'/api/identificadorocumento/all' ;
        ajax.metodo = "GET";   
        idoc_json = ajax.private.json();               
        var oJson = JSON.parse( idoc_json ) ;
        

        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['iden_doc'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                transito_carga_cliente_iden_doc.appendChild(opt);                     
            }
            
        }

            

};






TransitoCarga.prototype.borrar_combo_cliente = function( obj  ) {                
    
    var transito_carga_cliente_iden_doc = document.getElementById( "transito_carga_cliente_iden_doc" );
    
    for ( i = (transito_carga_cliente_iden_doc.length -1); i >= 0; i--){                     
        var aborrar = transito_carga_cliente_iden_doc.options[i];
        aborrar.parentNode.removeChild(aborrar);
    }
    

};






TransitoCarga.prototype.usuario_change = function(   ) {                
    
        var transito_carga_cliente = document.getElementById('transito_carga_cliente');          
        var transito_carga_cliente_iden_numero = document.getElementById('transito_carga_cliente_iden_numero'); 
        var transito_carga_cliente_iden_doc = document.getElementById('transito_carga_cliente_iden_doc'); 
        var transito_carga_cliente_iden_digito = document.getElementById('transito_carga_cliente_iden_digito'); 
        
        
        var  iden_numero = NumQP(fmtNum(transito_carga_cliente_iden_numero.value))
        var doc = transito_carga_cliente_iden_doc.value;
        var  digito = NumQP(fmtNum(transito_carga_cliente_iden_digito.value))        

    
        ajax.url = html.url.absolute()+'/api/clientes/'+doc+'/'+iden_numero+'/'+digito ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();      
        
        
        if (ajax.state == 200)
        {   
            var oJson = JSON.parse(datajson) ;         
            
            transito_carga_cliente.value = oJson['cliente'];            
        }  
        else{
            transito_carga_cliente.value = 0;
        }
        transito_carga_cliente.onblur();
    
    
    
    

};





TransitoCarga.prototype.post_form_id = function( obj, id ) { 
    

        var oJson = JSON.parse(form.json) ; 
        
        var transito_carga_cliente = document.getElementById('transito_carga_cliente');         
        transito_carga_cliente.value = oJson['cliente']['cliente']; 
        transito_carga_cliente.onblur();
        
        
        var transito_carga_vehiculo = document.getElementById('transito_carga_vehiculo');         
        transito_carga_vehiculo.value = oJson['vehiculo']['vehiculo']; 
        transito_carga_vehiculo.onblur();
    
    
    
//        obj.form_ini();
        obj.boolenEntregado();


        
};







TransitoCarga.prototype.preedit = function( obj ) { 
    

    
    document.getElementById('transito_carga_cliente_nombre').disabled = true;
    
    document.getElementById('transito_carga_marca').disabled = true;
    document.getElementById('transito_carga_modelo').disabled = true;
    document.getElementById('transito_carga_agno').disabled = true;
    document.getElementById('transito_carga_chasis').disabled = true;
    
    // bool yes no
    
    
};





TransitoCarga.prototype.boolenEntregado = function( ) { 
    

        var cam_fecha_entrega = document.getElementById('cam_fecha_entrega');  

        var entregado_false = document.getElementById('entregado_false');      
        entregado_false.onchange  = function(){                      
     
                if (entregado_false.checked){
                    cam_fecha_entrega.style.display = "none";
                }
                 else{
                     cam_fecha_entrega.style.display = "flex";
                 }
        }
        entregado_false.onchange();
    
        var entregado_true = document.getElementById('entregado_true');      
        entregado_true.onchange  = function(){                      
     
                if (entregado_true.checked){
                    cam_fecha_entrega.style.display = "flex";
                }
                 else{
                     cam_fecha_entrega.style.display = "none";
                 }
        }
        entregado_true.onchange();

    
};



