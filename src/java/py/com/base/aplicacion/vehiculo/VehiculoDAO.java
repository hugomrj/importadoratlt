/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.vehiculo;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class VehiculoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public VehiculoDAO ( ) throws IOException  {
    }
      
    
    public List<Vehiculo>  list (Integer page) {
                
        List<Vehiculo>  lista = null;        
        try {                        
                        
            VehiculoRS rs = new VehiculoRS();            
            
            
            lista = new Coleccion<Vehiculo>().resultsetToList(
                    new Vehiculo(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<Vehiculo>  search (Integer page, String busqueda) {
                
        List<Vehiculo>  lista = null;
        
        try {                       
                        
            VehiculoRS rs = new VehiculoRS();
            lista = new Coleccion<Vehiculo>().resultsetToList(
                    new Vehiculo(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    
}
