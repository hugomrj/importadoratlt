/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.vehiculo;

import py.com.base.aplicacion.vehiculomarca.VehiculoMarca;


/**
 *
 * @author hugom_000
 */
public class Vehiculo {


    private Integer vehiculo;
    private VehiculoMarca marca;
    private String modelo;
    private Integer fabricacion_agno;
    private String chasis_numero;
    private String color;
    private Integer carga;

    public Integer getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Integer vehiculo) {
        this.vehiculo = vehiculo;
    }

    public VehiculoMarca getMarca() {
        return marca;
    }

    public void setMarca(VehiculoMarca marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getFabricacion_agno() {
        return fabricacion_agno;
    }

    public void setFabricacion_agno(Integer fabricacion_agno) {
        this.fabricacion_agno = fabricacion_agno;
    }

    public String getChasis_numero() {
        return chasis_numero;
    }

    public void setChasis_numero(String chasis_numero) {
        this.chasis_numero = chasis_numero;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getCarga() {
        return carga;
    }

    public void setCarga(Integer carga) {
        this.carga = carga;
    }
    
    
}
