/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.cajachica_gasto;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class CajaChicaGastoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public CajaChicaGastoDAO ( ) throws IOException  {
    }
      
    
    public List<CajaChicaGasto>  list (Integer page) {
                
        List<CajaChicaGasto>  lista = null;        
        try {                        
                        
            CajaChicaGastoRS rs = new CajaChicaGastoRS();                 
            lista = new Coleccion<CajaChicaGasto>().resultsetToList(
                    new CajaChicaGasto(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<CajaChicaGasto>  search (Integer page, String busqueda) {
                
        List<CajaChicaGasto>  lista = null;        
        try {                       
                        
            CajaChicaGastoRS rs = new CajaChicaGastoRS();
            lista = new Coleccion<CajaChicaGasto>().resultsetToList(
                    new CajaChicaGasto(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    
}
