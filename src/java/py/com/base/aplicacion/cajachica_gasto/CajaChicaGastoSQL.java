/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.cajachica_gasto;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class CajaChicaGastoSQL {
    
        
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new CajaChicaGasto(), busqueda);        
        
        return sql ;             
    }        
       
    
    
    
    public String list (  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CajaChicaGasto");
        reader.fileExt = "list.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
        
    
}
