/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.cajachica_gasto;

import java.util.Date;
import py.com.base.aplicacion.categorias_gastos.CategoriaGasto;



/**
 *
 * @author hugo
 */
public class CajaChicaGasto {
    
    private Integer gasto;
    private Date fecha;
    private String descripcion;
    private String factura_numero;
    private CategoriaGasto cat_gasto;
    private Long importe;
    private Integer vehiculo;
    private Integer subcat_gasto;
    
    
    public Integer getGasto() {
        return gasto;
    }

    public void setGasto(Integer gasto) {
        this.gasto = gasto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFactura_numero() {
        return factura_numero;
    }

    public void setFactura_numero(String factura_numero) {
        this.factura_numero = factura_numero;
    }

    public CategoriaGasto getCat_gasto() {
        return cat_gasto;
    }

    public void setCat_gasto(CategoriaGasto cat_gasto) {
        this.cat_gasto = cat_gasto;
    }

    public Long getImporte() {
        return importe;
    }

    public void setImporte(Long importe) {
        this.importe = importe;
    }

    public Integer getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Integer vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Integer getSubcat_gasto() {
        return subcat_gasto;
    }

    public void setSubcat_gasto(Integer subcat_gasto) {
        this.subcat_gasto = subcat_gasto;
    }
    
}

