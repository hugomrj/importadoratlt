/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.caja_gasto;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class CajaGastoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public CajaGastoDAO ( ) throws IOException  {
    }
      
    
    public List<CajaGasto>  list (Integer page) {
                
        List<CajaGasto>  lista = null;        
        try {                        
                        
            CajaGastoRS rs = new CajaGastoRS();            
            
            
            lista = new Coleccion<CajaGasto>().resultsetToList(
                    new CajaGasto(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<CajaGasto>  search (Integer page, String busqueda) {
                
        List<CajaGasto>  lista = null;
        
        try {                       
                        
            CajaGastoRS rs = new CajaGastoRS();
            lista = new Coleccion<CajaGasto>().resultsetToList(
                    new CajaGasto(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    
}
