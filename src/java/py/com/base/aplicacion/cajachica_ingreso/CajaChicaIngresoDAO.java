/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.cajachica_ingreso;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class CajaChicaIngresoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public CajaChicaIngresoDAO ( ) throws IOException  {
    }
      
    
    public List<CajaChicaIngreso>  list (Integer page) {
                
        List<CajaChicaIngreso>  lista = null;        
        try {                        
                        
            CajaChicaIngresoRS rs = new CajaChicaIngresoRS();            
            
            
            lista = new Coleccion<CajaChicaIngreso>().resultsetToList(
                    new CajaChicaIngreso(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<CajaChicaIngreso>  search (Integer page, String busqueda) {
                
        List<CajaChicaIngreso>  lista = null;
        
        try {                       
                        
            CajaChicaIngresoRS rs = new CajaChicaIngresoRS();
            lista = new Coleccion<CajaChicaIngreso>().resultsetToList(
                    new CajaChicaIngreso(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    
}
