/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.cajachica_ingreso;


import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class CajaChicaIngresoSQL {
    
        
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new CajaChicaIngreso(), busqueda);        
        
        return sql ;             
    }        
       
    
    
    
    public String list (  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CajaChicaIngreso");
        reader.fileExt = "list.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
        
    
}
