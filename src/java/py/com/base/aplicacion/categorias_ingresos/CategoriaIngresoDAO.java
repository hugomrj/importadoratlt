/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.categorias_ingresos;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


/**
 *
 * @author hugom_000
 */

public class CategoriaIngresoDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public CategoriaIngresoDAO ( ) throws IOException  {
    }
      
    
    public List<CategoriaIngreso>  list (Integer page) {
                
        List<CategoriaIngreso>  lista = null;        
        try {                        
                        
            CategoriaIngresoRS rs = new CategoriaIngresoRS();            
            lista = new Coleccion<CategoriaIngreso>().resultsetToList(
                    new CategoriaIngreso(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<CategoriaIngreso>  search (Integer page, String busqueda) {
                
        List<CategoriaIngreso>  lista = null;
        
        try {                       
                        
            CategoriaIngresoRS rs = new CategoriaIngresoRS();
            lista = new Coleccion<CategoriaIngreso>().resultsetToList(
                    new CategoriaIngreso(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    public List<CategoriaIngreso>  all () {
                
        List<CategoriaIngreso>  lista = null;        
        try {                        
                        
            CategoriaIngresoRS rs = new CategoriaIngresoRS();            
            lista = new Coleccion<CategoriaIngreso>().resultsetToList(
                    new CategoriaIngreso(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
        
              
        
    
    
}
