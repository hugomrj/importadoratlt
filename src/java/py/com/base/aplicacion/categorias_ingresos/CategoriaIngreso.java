/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.categorias_ingresos;

/**
 *
 * @author hugo
 */
public class CategoriaIngreso {
    
    private Integer cat_ingreso;
    private String descripcion;

    public Integer getCat_ingreso() {
        return cat_ingreso;
    }

    public void setCat_ingreso(Integer cat_ingreso) {
        this.cat_ingreso = cat_ingreso;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}

