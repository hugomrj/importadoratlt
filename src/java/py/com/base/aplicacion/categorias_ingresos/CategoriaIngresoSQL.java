
package py.com.base.aplicacion.categorias_ingresos;


import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

public class CategoriaIngresoSQL {
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new CategoriaIngreso(), busqueda);        
        
        return sql ;             
    }        
    
    
    
    
    public String all (  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CategoriaIngreso");
        reader.fileExt = "all.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
            
    
    
    
    
}




