/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.vehiculomarca;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class VehiculoMarcaSQL {
    
    
    
    public String all (  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("VehiculoMarca");
        reader.fileExt = "all.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
    
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new VehiculoMarca(), busqueda);        
        
        return sql ;             
    }        
        
    
    
    
}
