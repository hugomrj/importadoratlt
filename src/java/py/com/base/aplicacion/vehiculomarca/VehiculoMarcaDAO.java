/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.vehiculomarca;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class VehiculoMarcaDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public VehiculoMarcaDAO ( ) throws IOException  {
    }
      

    public List<VehiculoMarca>  all () {
                
        List<VehiculoMarca>  lista = null;        
        try {                        
                        
            VehiculoMarcaRS rs = new VehiculoMarcaRS();            
            lista = new Coleccion<VehiculoMarca>().resultsetToList(
                    new VehiculoMarca(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
    
    public List<VehiculoMarca>  list (Integer page) {
                
        List<VehiculoMarca>  lista = null;        
        try {                        
                        
            VehiculoMarcaRS rs = new VehiculoMarcaRS();            
            lista = new Coleccion<VehiculoMarca>().resultsetToList(
                    new VehiculoMarca(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<VehiculoMarca>  search (Integer page, String busqueda) {
                
        List<VehiculoMarca>  lista = null;
        
        try {                       
                        
            VehiculoMarcaRS rs = new VehiculoMarcaRS();
            lista = new Coleccion<VehiculoMarca>().resultsetToList(
                    new VehiculoMarca(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
        
    
    
}
