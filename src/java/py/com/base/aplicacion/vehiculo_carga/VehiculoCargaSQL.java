
package py.com.base.aplicacion.vehiculo_carga;

import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;


public class VehiculoCargaSQL {
    
    


    
    public String list  ( ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("VehiculoCarga");
        readerSQL.fileExt = "list.sql";

        sql = readerSQL.get( );                      
        return sql ;
    }
    
    
    public String list_entransito  ( ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("VehiculoCarga");
        readerSQL.fileExt = "list_entransito.sql";

        sql = readerSQL.get( );                      
        return sql ;
    }
    


    
    public String list_entransito_search  ( String buscar ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("VehiculoCarga");
        readerSQL.fileExt = "list_entransito_search.sql";

        sql = readerSQL.get( buscar );                      
        return sql ;
    }
    
        
    
                
    
}




