/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.base.aplicacion.identificador_documento;

public class IdentificadorDocumento {
    
    private String iden_doc;
    private String descripcion;

    public String getIden_doc() {
        return iden_doc;
    }

    public void setIden_doc(String iden_doc) {
        this.iden_doc = iden_doc;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    
}
