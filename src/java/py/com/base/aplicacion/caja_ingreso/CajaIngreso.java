/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.caja_ingreso;


import java.util.Date;

import py.com.base.aplicacion.categorias_ingresos.CategoriaIngreso;
import py.com.base.aplicacion.vehiculo.Vehiculo;



/**
 *
 * @author hugo
 */
public class CajaIngreso {
    
    private Integer ingreso;
    private Date fecha;
    private String descripcion;    
    private CategoriaIngreso cat_ingreso;
    private Long importe;
    private Vehiculo vehiculo;

    public Integer getIngreso() {
        return ingreso;
    }

    public void setIngreso(Integer ingreso) {
        this.ingreso = ingreso;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public CategoriaIngreso getCat_ingreso() {
        return cat_ingreso;
    }

    public void setCat_ingreso(CategoriaIngreso cat_ingreso) {
        this.cat_ingreso = cat_ingreso;
    }

    public Long getImporte() {
        return importe;
    }

    public void setImporte(Long importe) {
        this.importe = importe;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
}

