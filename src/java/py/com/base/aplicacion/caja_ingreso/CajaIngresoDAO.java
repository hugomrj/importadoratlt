/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.caja_ingreso;



import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class CajaIngresoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public CajaIngresoDAO ( ) throws IOException  {
    }
      
    
    public List<CajaIngreso>  list (Integer page) {
                
        List<CajaIngreso>  lista = null;        
        try {                        
                        
            CajaIngresoRS rs = new CajaIngresoRS();            
            
            
            lista = new Coleccion<CajaIngreso>().resultsetToList(
                    new CajaIngreso(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<CajaIngreso>  search (Integer page, String busqueda) {
                
        List<CajaIngreso>  lista = null;
        
        try {                       
                        
            CajaIngresoRS rs = new CajaIngresoRS();
            lista = new Coleccion<CajaIngreso>().resultsetToList(
                    new CajaIngreso(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    
}
