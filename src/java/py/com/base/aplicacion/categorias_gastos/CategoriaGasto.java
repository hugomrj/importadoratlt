/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.categorias_gastos;

/**
 *
 * @author hugo
 */
public class CategoriaGasto {
    
    private Integer cat_gasto;
    private String descripcion;

    public Integer getCat_gasto() {
        return cat_gasto;
    }

    public void setCat_gasto(Integer cat_gasto) {
        this.cat_gasto = cat_gasto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    
    
}
