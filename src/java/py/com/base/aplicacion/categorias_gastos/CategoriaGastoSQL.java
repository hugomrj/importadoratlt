
package py.com.base.aplicacion.categorias_gastos;


import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

public class CategoriaGastoSQL {
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new CategoriaGasto(), busqueda);        
        
        return sql ;             
    }        
  
    
    
    
    
    public String all (  )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("CategoriaGasto");
        reader.fileExt = "all.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
        
    
}




