/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.subcategorias_gastos;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.SentenciaSQL;
import nebuleuse.ORM.xml.Global;




public class SubCategoriaGastoRS  {
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        

        
    public SubCategoriaGastoRS ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }
   

    

    public ResultSet  categoria ( Integer categoria ) 
            throws Exception {
        
            statement = conexion.getConexion().createStatement();       
            
            String sql = new SubCategoriaGastoSQL().categoria(categoria);
            
            //this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            //sql = sql + BasicSQL.limite_offset(page, lineas);            
            
            resultset = statement.executeQuery(sql);                 
            
            conexion.desconectar();                
            return resultset;                 
            
    }
   


    
    
}
