
package py.com.base.comercial.cliente;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

public class ClienteSQL {
    
    
    /*
    public String list ( String fecha )
            throws Exception {
    
        String sql = "";                         
        ReaderSQL readerSQL = new ReaderSQL("Agenda");
        
        readerSQL.fileSQL = "lista.sql";
        sql = readerSQL.getSQL( fecha );
        
        return sql ;             
    }        
       */ 
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new Cliente(), busqueda);        
        
        return sql ;             
    }        
    
    
    public String identificador ( String identi )
            throws Exception {
                
        String sql = "";                                 
        ReaderT reader = new ReaderT("Cliente");
        
        reader.fileExt = "identificador.sql";        
        sql = reader.get( identi );    
        
        return sql ;             
    }        
    
    
    
    public String getClienteDocNumero  (String tipo, String numero, Integer digito )
            throws Exception {

        
        String sql = "";                                 
        ReaderT reader = new ReaderT("Cliente");
        
        reader.fileExt = "getClienteDocNumero.sql";        
        sql = reader.get( tipo, numero, digito );    
        
        
        return sql ;             
    }        
    
    
    
    
}




