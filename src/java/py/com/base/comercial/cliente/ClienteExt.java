/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.cliente;

/**
 *
 * @author hugo
 */

public class ClienteExt extends Cliente{
    
    private String numeroidencliente;
    
    
    public void extender() {
        
    
        
        if (this.getIden_doc().getIden_doc().equals("R")) {                        
            String s = this.getIden_numero() + "-"  + this.getIden_digito().toString();             
            this.setNumeroidencliente(s);
        }
        else{
            if (this.getIden_doc().getIden_doc().equals("C")) {                                        
                this.setNumeroidencliente(this.getIden_numero());
            }
        }
        
    }    


    public String getNumeroidencliente() {
        return numeroidencliente;
    }

    public void setNumeroidencliente(String numeroidencliente) {
        this.numeroidencliente = numeroidencliente;
    }
    

            
            
}
