/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.transito_costo;

import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class TransitoCostoSQL {
    
        
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new TransitoCosto(), busqueda);        
        
        return sql ;             
    }        
       

    
    public String list  ( ) throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("TransitoCosto");
        readerSQL.fileExt = "list.sql";

        sql = readerSQL.get( );                      
        return sql ;
    }
    

    
}
