/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.transito_costo;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class TransitoCostoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public TransitoCostoDAO ( ) throws IOException  {
    }
      
    
    
    public List<TransitoCosto>  list (Integer page) {
                
        List<TransitoCosto>  lista = null;        
        try {                        
                        
            TransitoCostoRS rs = new TransitoCostoRS();            
            
            
            lista = new Coleccion<TransitoCosto>().resultsetToList(
                    new TransitoCosto(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<TransitoCosto>  search (Integer page, String busqueda) {
                
        List<TransitoCosto>  lista = null;
        
        try {                       
                        
            TransitoCostoRS rs = new TransitoCostoRS();
            lista = new Coleccion<TransitoCosto>().resultsetToList(
                    new TransitoCosto(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
    
    
    
    

    
}
