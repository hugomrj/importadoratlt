/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.transito_costo;

import py.com.base.comercial.transito_carga.TransitoCarga;

/**
 *
 * @author hugo
 */


public class TransitoCosto {
    
    private Integer transito_costo;    
    private TransitoCarga transito_carga;
    private Long costo_origen;
    private Long flete;
    
    private Long chapista;
    private Long mecanico;
    private Long accesorios_adicionales;
    private Long garantias_pagadas;
    private Long electricista;
    
    
    private Long despacho;
    private Long chapa;
    private Long comision_vendedor;
    private Long precio_venta;
    private Long monto_entrega;
    private Long diferencia_guardar;
    private Long nos_debe;
    private Long pago;
    private Long costo_total;
    private Long pago_venta_final;
    private Long comision_asesor;
    private Long utilidad_bruta;
    private Long proporcional_gasto;
    private Long utilidad_neta;

    
    
    public Integer getTransito_costo() {
        return transito_costo;
    }

    public void setTransito_costo(Integer transito_costo) {
        this.transito_costo = transito_costo;
    }

    public TransitoCarga getTransito_carga() {
        return transito_carga;
    }

    public void setTransito_carga(TransitoCarga transito_carga) {
        this.transito_carga = transito_carga;
    }

    public Long getCosto_origen() {
        return costo_origen;
    }

    public void setCosto_origen(Long costo_origen) {
        this.costo_origen = costo_origen;
    }

    public Long getFlete() {
        return flete;
    }

    public void setFlete(Long flete) {
        this.flete = flete;
    }


    public Long getDespacho() {
        return despacho;
    }

    public void setDespacho(Long despacho) {
        this.despacho = despacho;
    }

    public Long getChapa() {
        return chapa;
    }

    public void setChapa(Long chapa) {
        this.chapa = chapa;
    }


    public Long getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(Long precio_venta) {
        this.precio_venta = precio_venta;
    }

    public Long getMonto_entrega() {
        return monto_entrega;
    }

    public void setMonto_entrega(Long monto_entrega) {
        this.monto_entrega = monto_entrega;
    }

    public Long getDiferencia_guardar() {
        return diferencia_guardar;
    }

    public void setDiferencia_guardar(Long diferencia_guardar) {
        this.diferencia_guardar = diferencia_guardar;
    }

    public Long getNos_debe() {
        return nos_debe;
    }

    public void setNos_debe(Long nos_debe) {
        this.nos_debe = nos_debe;
    }

    public Long getPago() {
        return pago;
    }

    public void setPago(Long pago) {
        this.pago = pago;
    }

    public Long getCosto_total() {
        return costo_total;
    }

    public void setCosto_total(Long costo_total) {
        this.costo_total = costo_total;
    }

    public Long getPago_venta_final() {
        return pago_venta_final;
    }

    public void setPago_venta_final(Long pago_venta_final) {
        this.pago_venta_final = pago_venta_final;
    }

    public Long getComision_asesor() {
        return comision_asesor;
    }

    public void setComision_asesor(Long comision_asesor) {
        this.comision_asesor = comision_asesor;
    }

    public Long getUtilidad_bruta() {
        return utilidad_bruta;
    }

    public void setUtilidad_bruta(Long utilidad_bruta) {
        this.utilidad_bruta = utilidad_bruta;
    }

    public Long getProporcional_gasto() {
        return proporcional_gasto;
    }

    public void setProporcional_gasto(Long proporcional_gasto) {
        this.proporcional_gasto = proporcional_gasto;
    }

    public Long getUtilidad_neta() {
        return utilidad_neta;
    }

    public void setUtilidad_neta(Long utilidad_neta) {
        this.utilidad_neta = utilidad_neta;
    }

    public Long getComision_vendedor() {
        return comision_vendedor;
    }

    public void setComision_vendedor(Long comision_vendedor) {
        this.comision_vendedor = comision_vendedor;
    }

    public Long getChapista() {
        return chapista;
    }

    public void setChapista(Long chapista) {
        this.chapista = chapista;
    }

    public Long getMecanico() {
        return mecanico;
    }

    public void setMecanico(Long mecanico) {
        this.mecanico = mecanico;
    }

    public Long getAccesorios_adicionales() {
        return accesorios_adicionales;
    }

    public void setAccesorios_adicionales(Long accesorios_adicionales) {
        this.accesorios_adicionales = accesorios_adicionales;
    }

    public Long getGarantias_pagadas() {
        return garantias_pagadas;
    }

    public void setGarantias_pagadas(Long garantias_pagadas) {
        this.garantias_pagadas = garantias_pagadas;
    }

    public Long getElectricista() {
        return electricista;
    }

    public void setElectricista(Long electricista) {
        this.electricista = electricista;
    }
    
    
            
            

    
}
