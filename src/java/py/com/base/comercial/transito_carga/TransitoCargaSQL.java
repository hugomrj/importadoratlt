/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.transito_carga;


import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class TransitoCargaSQL {
    
        
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new TransitoCarga(), busqueda);        
        
        return sql ;             
    }        
       
    
    
    
    public String totalvehiculos  ( ) throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("TransitoCarga");
        readerSQL.fileExt = "totalvehiculos.sql";

        sql = readerSQL.get(  );
        
        return sql ;                     
        
    }
    
        
    
    
    
}
