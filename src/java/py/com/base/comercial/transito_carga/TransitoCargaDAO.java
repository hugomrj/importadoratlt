/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.transito_carga;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;
import nebuleuse.util.Datetime;
import py.com.base.aplicacion.vehiculo.Vehiculo;
import py.com.base.comercial.cliente.Cliente;

/**
 *
 * @author hugom_000
 */
public class TransitoCargaDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public TransitoCargaDAO ( ) throws IOException  {
    }
      
    
    public List<TransitoCarga>  list (Integer page) {
                
        List<TransitoCarga>  lista = null;        
        try {                        
                        
            TransitoCargaRS rs = new TransitoCargaRS();            
            
            
            lista = new Coleccion<TransitoCarga>().resultsetToList(
                    new TransitoCarga(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<TransitoCarga>  search (Integer page, String busqueda) {
                
        List<TransitoCarga>  lista = null;
        
        try {                       
                        
            TransitoCargaRS rs = new TransitoCargaRS();
            lista = new Coleccion<TransitoCarga>().resultsetToList(
                    new TransitoCarga(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
    
    
    
    public TransitoCarga  fromJson (String json) {
                
                TransitoCarga transitoCarga = new TransitoCarga();   
                
                Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
                                             
                JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);

                transitoCarga.setTransito_carga(  jsonObject.get("transito_carga").getAsInt()  );
                transitoCarga.setBuque_nombre( jsonObject.get("buque_nombre").getAsString() );

                Cliente cliente = new Cliente();
                cliente.setCliente(jsonObject.get("cliente").getAsInt()  );
                transitoCarga.setCliente(cliente);

                transitoCarga.setFecha_llegada_iqq(                                         
                    Datetime.castDate(  jsonObject.get("fecha_llegada_iqq").toString()  )                                        
                );

                transitoCarga.setFecha_llegada_py(
                    Datetime.castDate(  jsonObject.get("fecha_llegada_py").toString()  )                                        
                );

                transitoCarga.setFecha_salida(
                    Datetime.castDate(  jsonObject.get("fecha_salida").toString()  )                                        
                );

                transitoCarga.setFecha_transacion(
                    Datetime.castDate(  jsonObject.get("fecha_transacion").toString()  )                                        
                );

                Vehiculo vehiculo = new Vehiculo();
                vehiculo.setVehiculo(  jsonObject.get("vehiculo").getAsInt()  );            
                transitoCarga.setVehiculo(vehiculo);

                
     return transitoCarga;


    }          
    
        
    
    
    
    

    
}
