/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.transito_carga;

import java.util.Date;
import py.com.base.aplicacion.vehiculo.Vehiculo;
import py.com.base.comercial.cliente.Cliente;

/**
 *
 * @author hugo
 */


public class TransitoCarga {
    
    private Integer transito_carga;
    private Cliente cliente;
    private Vehiculo vehiculo;
    private String buque_nombre;
    private Date fecha_transacion;
    private Date fecha_salida;
    private Date fecha_llegada_iqq;
    private Date fecha_llegada_py;
    private Boolean entregado ;
    private Date fecha_entrega;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getBuque_nombre() {
        return buque_nombre;
    }

    public void setBuque_nombre(String buque_nombre) {
        this.buque_nombre = buque_nombre;
    }

    public Date getFecha_transacion() {
        return fecha_transacion;
    }

    public void setFecha_transacion(Date fecha_transacion) {
        this.fecha_transacion = fecha_transacion;
    }

    public Date getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(Date fecha_salida) {
        this.fecha_salida = fecha_salida;
    }

    public Date getFecha_llegada_iqq() {
        return fecha_llegada_iqq;
    }

    public void setFecha_llegada_iqq(Date fecha_llegada_iqq) {
        this.fecha_llegada_iqq = fecha_llegada_iqq;
    }

    public Date getFecha_llegada_py() {
        return fecha_llegada_py;
    }

    public void setFecha_llegada_py(Date fecha_llegada_py) {
        this.fecha_llegada_py = fecha_llegada_py;
    }

    public Integer getTransito_carga() {
        return transito_carga;
    }

    public void setTransito_carga(Integer transito_carga) {
        this.transito_carga = transito_carga;
    }

    public Boolean getEntregado() {
        return entregado;
    }

    public void setEntregado(Boolean entregado) {
        this.entregado = entregado;
    }

    public Date getFecha_entrega() {
        return fecha_entrega;
    }

    public void setFecha_entrega(Date fecha_entrega) {
        this.fecha_entrega = fecha_entrega;
    }
    
    
}
