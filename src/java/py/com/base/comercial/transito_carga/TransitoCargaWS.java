/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.transito_carga;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.sql.SQLException;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.Datetime;
import py.com.base.aplicacion.vehiculo.Vehiculo;
import py.com.base.comercial.cliente.Cliente;

/**
 * REST Web Service
 *
 * @author hugo
 */


@Path("transito_cargas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)




public class TransitoCargaWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    TransitoCarga com = new TransitoCarga();    


    public TransitoCargaWS() {
    }


    @GET    
    public Response list ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page) {
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                TransitoCargaDAO dao = new TransitoCargaDAO(); 
                
                List<TransitoCarga> lista = dao.list(page); 
                
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    





    
    
    
    @GET       
    //@Path("/search/{opt : (.*)}") 
    @Path("/search/") 
    public Response search ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,              
            @MatrixParam("q") String q
            ) {
        
        
            if (page == null) {                
                page = 1;
            }
            if (q == null){            
                q = "";                
            }
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                TransitoCargaDAO dao = new TransitoCargaDAO();
                
                List<TransitoCarga> lista = dao.search(page, q);
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();        
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    




    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                
                this.com = (TransitoCarga) persistencia.filtrarId(this.com, id);  
                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                return Response
                        .status( this.status )
                        .entity(this.com)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        
        
    }    

    
    
    

    
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
                     
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                             
                
                
                Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
                
                TransitoCarga transitoCarga = new TransitoCarga();                

                JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);

                transitoCarga.setTransito_carga(  jsonObject.get("transito_carga").getAsInt()  );
                transitoCarga.setBuque_nombre( jsonObject.get("buque_nombre").getAsString() );

                Cliente cliente = new Cliente();
                cliente.setCliente(jsonObject.get("cliente").getAsInt()  );
                transitoCarga.setCliente(cliente);

                transitoCarga.setFecha_llegada_iqq(                                         
                    Datetime.castDate(  jsonObject.get("fecha_llegada_iqq").toString()  )                                        
                );

                transitoCarga.setFecha_llegada_py(
                    Datetime.castDate(  jsonObject.get("fecha_llegada_py").toString()  )                                        
                );

                transitoCarga.setFecha_salida(
                    Datetime.castDate(  jsonObject.get("fecha_salida").toString()  )                                        
                );

                transitoCarga.setFecha_transacion(
                    Datetime.castDate(  jsonObject.get("fecha_transacion").toString()  )                                        
                );

                Vehiculo vehiculo = new Vehiculo();
                vehiculo.setVehiculo(  jsonObject.get("vehiculo").getAsInt()  );            
                transitoCarga.setVehiculo(vehiculo);
                
                transitoCarga.setEntregado(Boolean.FALSE);
                
                this.com = (TransitoCarga) persistencia.insert(transitoCarga);                    
                
                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }
                
                return Response
                        .status(this.status)
                        .entity(this.com)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else
            {                 
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();             
            }
        
        }     
        

        catch (SQLException ex) {     
            
            return Response
                    .status(Response.Status.BAD_GATEWAY)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();       
        }
        
        
        
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    





    @PUT    
    @Path("/{id}")    
    public Response edit (
            String json,
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
        
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                
                TransitoCargaDAO dao = new TransitoCargaDAO();
                
                TransitoCarga req = dao.fromJson(json);                
        
        JsonObject jsonObject = new Gson().fromJson( json, JsonObject.class );            
        JsonElement jsonElement = jsonObject.get("entregado");                
        req.setEntregado(jsonElement.getAsBoolean());
                
                req.setTransito_carga(id);
                this.com = (TransitoCarga) persistencia.update(req);
            
                return Response
                        .status(Response.Status.OK)
                        .entity(this.com)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
                throw new Exception();                
            }
        
        }     
        
        
        catch (SQLException ex) {     
            
            return Response
                    .status(Response.Status.BAD_GATEWAY)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();       
        }
        
                
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                    
    
        }        
    }    
    

    
    
    @DELETE  
    @Path("/{id}")    
    public Response delete (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                Integer filas = 0;
                filas = persistencia.delete(this.com, id) ;
                
                
                if (filas != 0){
                    
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{                    
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();          
                    
                    
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }  
        
    }    
    
    
    
    
}
