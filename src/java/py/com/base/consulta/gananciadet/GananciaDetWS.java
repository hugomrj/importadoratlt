/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.base.consulta.gananciadet;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;




@Path("gananciasdetalles")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class GananciaDetWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
                        
    public GananciaDetWS() {
    }

    
    
    
    
    @GET        
    @Path("/{agno}/{mes}")    
    public Response list ( 
            
            @PathParam ("mes") Integer mes,
            @PathParam ("agno") Integer agno,                 
            
            
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page) 
    {    

        
            if (page == null) {                
                page = 1;
            }
        
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
                
                GananciaDetJSON gananciadetjson = new GananciaDetJSON();
                
                JsonArray jsonarray = gananciadetjson.list( agno, mes, page );
                json = jsonarray.toString();                

                
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())           
                        .header("total_registros", gananciadetjson.total_registros )
                        .build();                       
            }
            else
            {
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
     
    
    
    
    
}
    
    
    
  