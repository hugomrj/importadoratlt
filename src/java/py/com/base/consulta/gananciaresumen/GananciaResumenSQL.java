
package py.com.base.consulta.gananciaresumen;

import py.com.base.consulta.gananciadet.*;
import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

public class GananciaResumenSQL {
    
    


    
    public String list  (  Integer agno, Integer mes  ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("GananciaResumen");
        readerSQL.fileExt = "lista.sql";

        sql = readerSQL.get( agno, mes );
        
        return sql ;                     
        
    }
    
    
    
    
    
    public String suma  (  Integer agno, Integer mes  ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("GananciaResumen");
        readerSQL.fileExt = "suma.sql";

        sql = readerSQL.get( agno, mes );
        
        return sql ;                     
        
    }
              
    
            
            
    
    public String saldoacumulado  (  Integer agno, Integer mes  ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("GananciaResumen");
        readerSQL.fileExt = "saldoacumulado.sql";

        
        
        sql = readerSQL.get( agno, mes );

        
        return sql ;                     
        
    }
                      
    
    
}




