/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.base.consulta.gananciaresumen;

import py.com.base.consulta.gananciadet.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;




@Path("gananciasresumen")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class GananciaResumentWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
                        
    public GananciaResumentWS() {
    }

    
    
    
    
    @GET        
    @Path("/{agno}/{mes}")    
    public Response list ( 
            
            @PathParam ("mes") Integer mes,
            @PathParam ("agno") Integer agno,                 
            
            
            @HeaderParam("token") String strToken) 
    {    

        /*
            if (page == null) {                
                page = 1;
            }
        */
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
                
                GananciaResumenJSON gananciadetjson = new GananciaResumenJSON();
                
                JsonArray jsonarray = gananciadetjson.list( agno, mes );
                json = jsonarray.toString();                

                
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())           
                       /* .header("total_registros", gananciadetjson.total_registros )*/
                        .build();                       
            }
            else
            {
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
     
    

    
    @GET        
    @Path("/suma/{agno}/{mes}")    
    public Response suma ( 
            
            @PathParam ("mes") Integer mes,
            @PathParam ("agno") Integer agno,                 
            
            
            @HeaderParam("token") String strToken) 
    {    

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
                
                GananciaResumenJSON gananciadetjson = new GananciaResumenJSON();
                
                JsonArray jsonarray = gananciadetjson.suma( agno, mes );
                json = jsonarray.toString();                

                
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())           
                       /* .header("total_registros", gananciadetjson.total_registros )*/
                        .build();                       
            }
            else
            {
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
     
    
    
    
    
    @GET        
    @Path("/saldoacumulado/{agno}/{mes}")    
    public Response saldoacumulado ( 
            
            @PathParam ("mes") Integer mes,
            @PathParam ("agno") Integer agno,                 
            
            
            @HeaderParam("token") String strToken) 
    {    

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
                
                GananciaResumenJSON gananciadetjson = new GananciaResumenJSON();
                
                JsonArray jsonarray = gananciadetjson.saldoacumulado( agno, mes );
                json = jsonarray.toString();                

                
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())           
                       /* .header("total_registros", gananciadetjson.total_registros )*/
                        .build();                       
            }
            else
            {
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
     
    
        
    
    
    
    
}
    
    
    
  