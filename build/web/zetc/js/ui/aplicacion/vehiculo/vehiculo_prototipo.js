

function Vehiculo(){
    
   this.tipo = "vehiculo";   
   this.recurso = "vehiculos";   
   this.value = 0;
   this.form_descrip = "vehiculo_descripcion";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
      
   this.titulosin = "Vehiculo"
   this.tituloplu = "Vehiculos"   
      
   
   this.campoid=  'vehiculo';
   this.tablacampos =  ['vehiculo', 'marca.descripcion', 'modelo', 'chasis_numero', 'color',
       'fabricacion_agno', ];
   
    this.etiquetas =  ['Vehiculo', 'Marca', 'Modelo', 'Chasis', 'Color', 
        'Año' ];                                  
    
    this.tablaformat = ['C', 'C', 'C', 'C', 'C', 'N'];                                  
   
   this.tbody_id = "vehiculo-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "vehiculo-acciones";   
      
   
   this.parent = null;
   
   
    this.combobox = {"marca":{
         "value":"marca",
         "inner":"descripcion"}};   
   
}


Vehiculo.prototype.lista_new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        

    
};



Vehiculo.prototype.form_ini = function() {    
    
};



Vehiculo.prototype.form_validar = function() {    
    return true;
};







Vehiculo.prototype.carga_combos = function( obj  ) {                
    
   
   
        var vehiculo_marca = document.getElementById("vehiculo_marca");
        var idedovalue = vehiculo_marca.value;
   
        ajax.url = html.url.absolute()+'/api/vehiculosmarcas/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();               
                
        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['marca'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                vehiculo_marca.appendChild(opt);                     
            }
            
        }
            

};

