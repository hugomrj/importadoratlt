

function CajaChicaGasto(){
    
    this.tipo = "cajachicagasto";   
    this.recurso = "cajachicagastos";   
    this.carpeta=  "/aplicacion";   
    
    
   this.titulosin = "Gasto"
   this.tituloplu = "Gastos"       
    
        
    this.campoid  = 'gasto';
    this.tablacampos = ['fecha', 'descripcion', 'factura_numero', 'cat_gasto.descripcion', 'importe'];        
    this.etiquetas = ['fecha', 'descripcion', 'factura_numero', 'categoria', 'importe'];        
        
   
   this.tablaformat =  ['D', 'C', 'C', 'C', 'N' ];                   
   
    
    this.botones_lista = "cajachicagasto_acciones_lista";
    this.tablacamposoculto = [ ];    
    
    
    this.combobox = {"cat_gasto":{
         "value":"cat_gasto",
         "inner":"descripcion"}};       

   this.modal_ancho = 800;
   
};




CajaChicaGasto.prototype.form_ini = function() {    
    
    var cajagasto_importe = document.getElementById('cajachicagasto_importe');          
    cajagasto_importe.onblur  = function() {                
        cajagasto_importe.value  = fmtNum(cajagasto_importe.value);
    };     
    cajagasto_importe.onblur();          
   
    
};



CajaChicaGasto.prototype.lista_new = function( obj  ) {        
    alert("lista_new");
};




CajaChicaGasto.prototype.modal_new = function(  obj  ) {    
 
 
    modal.ancho = 800;       
    
    modal.form.new( obj );  

    // detalles del 
    var cam_vehiculo = document.getElementById('cam_vehiculo');
    var cam_vehiculo02 = document.getElementById('cam_vehiculo02');
    var subcate = document.getElementById('subcate');          
    
    
    cam_vehiculo.style.display = "none";  
    cam_vehiculo02.style.display = "none";  
    subcate.style.display = "none";  
    

    // mostrar vehiculo 
    var cajachicagasto_cat_gasto = document.getElementById('cajachicagasto_cat_gasto');
    cajachicagasto_cat_gasto.onchange  = function(){                      
        //consulta_datos ();            
        var ccvalue = cajachicagasto_cat_gasto.value;
        
        
        if (ccvalue == 3){            
            cam_vehiculo.style.display = "flex";  
            cam_vehiculo02.style.display = "flex";  
            subcate.style.display = "flex";  
            
            // funciones 
            form_nuevo_gasto_subconsulta_acciones ( );
            
        }
        else{
            cam_vehiculo.style.display = "none";  
            cam_vehiculo02.style.display = "none";  
            subcate.style.display = "none";  
        }
        
    }                         
    cajachicagasto_cat_gasto.onchange();
          
    
    


   
};





CajaChicaGasto.prototype.tabuladores = function( cajamayor ) {  
    
};
    
    
    




CajaChicaGasto.prototype.form_validar = function() {    

    var cajagasto_fecha = document.getElementById('cajachicagasto_fecha');
    if (cajagasto_fecha.value == ""){
        msg.error.mostrar("Error en Fecha ");                    
        cajagasto_fecha.focus();
        cajagasto_fecha.select();                                       
        return false;        
    }
    
        
    
    var cajagasto_importe = document.getElementById('cajachicagasto_importe');        
    if (parseInt(NumQP(cajagasto_importe.value)) <=  0 )         
    {
        msg.error.mostrar("Falta valor en importe");   
        cajagasto_importe.focus();
        cajagasto_importe.select();     
        return false;
    }  
       
    
    return true;
};




CajaChicaGasto.prototype.sublista = function( obj ) {       
    
        tabla.setObjeto(obj);
        
        coleccion.lista.funciones =  [ obj.modal_new ] ;
        coleccion.lista.accion( obj );     
   

        var btn_cajachicagasto_agregar = document.getElementById('btn_cajachicagasto_agregar');
        btn_cajachicagasto_agregar.onclick = function(){
            obj.modal_new(obj);
        };   




        // paginacion
         document.getElementById( "cajachicagasto_paginacion" ).innerHTML 
         = paginacion.gene();    


            var buscar = "";        
//         verificacion_paginacion_move(obj, buscar, function (id) {edicion_verificacion ( id );} )

            paginacion.move(obj, buscar, function (obj, id) {modal.form.row (obj, id)}  );    
   
   
   
   
};








CajaChicaGasto.prototype.carga_combos = function( obj  ) {                
    
   
        var cajachicagasto_cat_gasto = document.getElementById("cajachicagasto_cat_gasto");
        var idedovalue = cajachicagasto_cat_gasto.value;
   
        ajax.url = html.url.absolute()+'/api/categoriagasto/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();               

               
        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['cat_gasto'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                cajachicagasto_cat_gasto.appendChild(opt);                     
            }
            
        }
            

};




CajaChicaGasto.prototype.modal_row = function( obj ) {       
    
    
    var cajachicagasto_cat_gasto = document.getElementById("cajachicagasto_cat_gasto");
    
    
    var cam_vehiculo = document.getElementById('cam_vehiculo');
    var cam_vehiculo02 = document.getElementById('cam_vehiculo02');
    var subcate = document.getElementById('subcate');   
    
    
            if (cajachicagasto_cat_gasto.value == 3){ 
                
                cam_vehiculo.style.display = "flex";  
                cam_vehiculo02.style.display = "flex";  
                subcate.style.display = "flex";  
            
                // funciones 
                form_nuevo_gasto_subconsulta_acciones ( );
            
            }
            else
            {
                cam_vehiculo.style.display = "none";  
                cam_vehiculo02.style.display = "none";  
                subcate.style.display = "none";  
            }







            if (cajachicagasto_cat_gasto.value == 3){ 

                var btn_cajachicagasto_modificar = document.getElementById("btn_cajachicagasto_modificar");                                
                btn_cajachicagasto_modificar.onclick = function(  )
                {  
                    msg.error.mostrar("La categoria Mercaderia en Transito no puede ser editada solo borrada");           
                };        

            }
            else
            {

                var btn_cajachicagasto_modificar = document.getElementById("btn_cajachicagasto_modificar");                                
                btn_cajachicagasto_modificar.onclick = function(  )
                {  
                    //modal.ventana.cerrar(obj.venactiva);                
                    modal.ventana.cerrar(obj.venactiva);
                    modal.form.edi(obj);  
                    
                    document.getElementById('cam_vehiculo').style.display = "none";  
                    document.getElementById('cam_vehiculo02').style.display = "none";  
                    document.getElementById('subcate').style.display = "none";  


                    var sel = document.getElementById('cajachicagasto_cat_gasto');
                    var selindex = 0;
                    for (var i = 0; i < sel.children.length; ++i) {

                        var child = sel.children[i];
                        if (child.value == 3){
                            selindex = i;
                        }
                    }
                    sel.remove(selindex);
                
                };        
                
            }


    //
    
    
    
    
                var btn_cajachicagasto_salir = document.getElementById("btn_cajachicagasto_salir");                                
                btn_cajachicagasto_salir.onclick = function(  )
                {  
                    modal.ventana.cerrar(obj.venactiva);                
                };        
    
    
    //btn_cajachicagasto_salir
    
    
    
    
};
