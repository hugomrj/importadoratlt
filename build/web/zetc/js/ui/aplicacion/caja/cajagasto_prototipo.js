

function CajaGasto(){
    
    this.tipo = "cajagasto";   
    this.recurso = "cajagastos";   
    this.carpeta=  "/aplicacion";   
    
    
   this.titulosin = "Gasto"
   this.tituloplu = "Gastos"       
    
        
    this.campoid  = 'gasto';
    this.tablacampos = ['fecha', 'descripcion', 'factura_numero', 'cat_gasto.descripcion', 'importe'];        
    this.etiquetas = ['fecha', 'descripcion', 'factura_numero', 'categoria', 'importe'];        
        
   
   this.tablaformat =  ['D', 'C', 'C', 'C', 'N' ];                   
        
    
    this.botones_lista = "cajagasto_acciones_lista";
    this.tablacamposoculto = [ ];    
    
    
    this.combobox = {"cat_gasto":{
         "value":"cat_gasto",
         "inner":"descripcion"}};       

    
};


CajaGasto.prototype.form_ini = function() {    
    
    var cajagasto_importe = document.getElementById('cajagasto_importe');          
    cajagasto_importe.onblur  = function() {                
        cajagasto_importe.value  = fmtNum(cajagasto_importe.value);
    };     
    cajagasto_importe.onblur();          
    
    
    
    
};



CajaGasto.prototype.lista_new = function( obj  ) {        
    alert("lista_new");
};




CajaGasto.prototype.modal_new = function(  obj  ) {    


    modal.form.new( obj );  
    
    

/*
    obj.foreign = [ new Rol(), new Sucursal() ] ;
    reflex.ui.foreign_more(obj);
    reflex.ui.foreign_plus(obj);
    reflex.ui.foreign_decrip(obj);

    modal.form.pasarcab(obj);
    modal.form.ocultarcab(obj);

    
    var caid = document.getElementById( "rolsucursal_id");
    caid.value = 0;    
    caid.disabled=true;
    */
   
};





CajaGasto.prototype.tabuladores = function( cajamayor ) {  
};
    
    
    




CajaGasto.prototype.form_validar = function() {    

    var cajagasto_fecha = document.getElementById('cajagasto_fecha');
    if (cajagasto_fecha.value == ""){
        msg.error.mostrar("Error en Fecha ");                    
        cajagasto_fecha.focus();
        cajagasto_fecha.select();                                       
        return false;        
    }
    
        
    
    var cajagasto_importe = document.getElementById('cajagasto_importe');        
    if (parseInt(NumQP(cajagasto_importe.value)) <=  0 )         
    {
        msg.error.mostrar("Falta valor en importe");   
        cajagasto_importe.focus();
        cajagasto_importe.select();     
        return false;
    }  
        
    

    
    return true;
};




CajaGasto.prototype.sublista = function( obj ) {       
    
        tabla.setObjeto(obj);
        
        coleccion.lista.funciones =  [ obj.modal_new ] ;
        coleccion.lista.accion( obj );     
        
    
  
};








CajaGasto.prototype.carga_combos = function( obj  ) {                
    
   
   
        var cajagasto_cat_gasto = document.getElementById("cajagasto_cat_gasto");
        var idedovalue = cajagasto_cat_gasto.value;
   
        ajax.url = html.url.absolute()+'/api/categoriagasto/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();               

               
        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['cat_gasto'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                cajagasto_cat_gasto.appendChild(opt);                     
            }
            
        }
            

};

