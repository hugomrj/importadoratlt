--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.19
-- Dumped by pg_dump version 9.5.19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: aplicacion; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA aplicacion;


ALTER SCHEMA aplicacion OWNER TO postgres;

--
-- Name: sistema; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sistema;


ALTER SCHEMA sistema OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: eliminar_caja_chica_gastos(); Type: FUNCTION; Schema: aplicacion; Owner: postgres
--

CREATE FUNCTION aplicacion.eliminar_caja_chica_gastos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE        	

	varcomando character varying;
	varcarga integer;
	

BEGIN



    IF (OLD.cat_gasto = 3 ) THEN       


	varcarga  := 
		(
		SELECT transito_costos.transito_costo
		FROM 
			aplicacion.vehiculos, aplicacion.transito_costos, aplicacion.transito_cargas
		WHERE 
			vehiculos.carga = transito_costos.transito_carga
			and transito_costos.transito_carga = transito_cargas.transito_carga 
			and entregado = false
			and vehiculos.vehiculo = OLD.vehiculo  ) ;

		IF (varcarga IS NULL) THEN
			RAISE EXCEPTION ' Error en vehiculo seleccionado, ya ha sido entregado o no pertenece a la carga  ';
		END IF;

	varcomando  := (
		SELECT comando
		FROM aplicacion.subcat_gastos
		where subcat = OLD.subcat_gasto
	);



--RAISE EXCEPTION 'varcomando % ', varcomando ;

	    EXECUTE 
		' UPDATE aplicacion.transito_costos 
		  SET ' || varcomando || ' =  0  
		  WHERE transito_costo  =  ' || varcarga||  ' ;';

		
--RAISE EXCEPTION 'comando % ', NEW.subcat_gasto;	

    END IF;


   RETURN OLD;
    
END;
$$;


ALTER FUNCTION aplicacion.eliminar_caja_chica_gastos() OWNER TO postgres;

--
-- Name: insertar_caja_chica_gastos(); Type: FUNCTION; Schema: aplicacion; Owner: postgres
--

CREATE FUNCTION aplicacion.insertar_caja_chica_gastos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE        	
	varcomando character varying;
	varcarga integer;	
	varmonto integer;	
BEGIN

    IF (NEW.cat_gasto = 3 ) THEN       

	varcarga  := 
		(
		SELECT transito_costos.transito_costo
		FROM 
			aplicacion.vehiculos, aplicacion.transito_costos, aplicacion.transito_cargas
		WHERE 
			vehiculos.carga = transito_costos.transito_carga
			and transito_costos.transito_carga = transito_cargas.transito_carga 
			and entregado = false
			and vehiculos.vehiculo = NEW.vehiculo  ) ;

 --raise info '%',varcarga;


		IF (varcarga IS NULL) THEN
			RAISE EXCEPTION ' Error en vehiculo seleccionado, ya ha sido entregado o no pertenece a la carga  ';
		END IF;		

	varcomando  := (
		SELECT comando
		FROM aplicacion.subcat_gastos
		where subcat = NEW.subcat_gasto
	);




      EXECUTE format('SELECT %s  FROM aplicacion.transito_costos  
			where  transito_costo  =  ' || varcarga ||  ' ;'
      , varcomando )
      INTO varmonto; 

	IF (varmonto != 0) THEN
		RAISE EXCEPTION ' Ya existe un importe cargado para %', varcomando ;
	END IF;	


	    EXECUTE 
		' UPDATE aplicacion.transito_costos 
		  SET ' || varcomando || ' = '   || NEW.importe || 
		' WHERE transito_costo  =  ' || varcarga||  ' ;';

		
--RAISE EXCEPTION 'comando % ', NEW.subcat_gasto;	
        
    END IF;


   RETURN NEW;
    
END;
$$;


ALTER FUNCTION aplicacion.insertar_caja_chica_gastos() OWNER TO postgres;

--
-- Name: insertar_transporte_costo_registro(); Type: FUNCTION; Schema: aplicacion; Owner: postgres
--

CREATE FUNCTION aplicacion.insertar_transporte_costo_registro() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE         
suma_rendicion bigint;
monto_transferencia  bigint; 
cod_carga integer; 

BEGIN



INSERT INTO aplicacion.transito_costos(transito_carga)
    VALUES ( NEW.transito_carga);



cod_carga :=  (
SELECT carga
FROM aplicacion.vehiculos
where vehiculo = NEW.vehiculo
);


    IF (cod_carga =  0 ) THEN       
        
UPDATE aplicacion.vehiculos
   SET  carga = NEW.transito_carga    
 WHERE vehiculo = NEW.vehiculo;        
        
    END IF;




    IF ( cod_carga !=  0 ) THEN      

RAISE EXCEPTION 'El vehiculo ya esta relacionada a una carga '; 
        
    END IF;



    

   RETURN NEW;
 
END;
$$;


ALTER FUNCTION aplicacion.insertar_transporte_costo_registro() OWNER TO postgres;

--
-- Name: update_campos_transporte_costo(text); Type: FUNCTION; Schema: aplicacion; Owner: postgres
--

CREATE FUNCTION aplicacion.update_campos_transporte_costo(field text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    value text;
BEGIN

value  := 0;

    EXECUTE 
        'UPDATE aplicacion.transito_costos 
	SET ' || field || ' = 4000  
	WHERE transito_costo  = 28;';
     

     

    return value;
END;
$$;


ALTER FUNCTION aplicacion.update_campos_transporte_costo(field text) OWNER TO postgres;

--
-- Name: update_transporte_carga_vehiculo(); Type: FUNCTION; Schema: aplicacion; Owner: postgres
--

CREATE FUNCTION aplicacion.update_transporte_carga_vehiculo() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE        
	cod_carga integer;	

BEGIN

	cod_carga :=  (
		SELECT carga
		FROM aplicacion.vehiculos
		where vehiculo = NEW.vehiculo
	);



	IF ( NEW.vehiculo != OLD.vehiculo ) THEN     
		RAISE EXCEPTION 'No se puede cambiar el vehiculo en una carga iniciada';
	END IF;
    
	

	
	
   RETURN NEW;
 
END;
$$;


ALTER FUNCTION aplicacion.update_transporte_carga_vehiculo() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: caja_chica_gastos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.caja_chica_gastos (
    gasto integer NOT NULL,
    fecha date,
    descripcion character varying,
    factura_numero character varying,
    cat_gasto integer,
    importe bigint,
    vehiculo integer,
    subcat_gasto integer DEFAULT 0
);


ALTER TABLE aplicacion.caja_chica_gastos OWNER TO postgres;

--
-- Name: caja_chica_gasto_gasto_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.caja_chica_gasto_gasto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.caja_chica_gasto_gasto_seq OWNER TO postgres;

--
-- Name: caja_chica_gasto_gasto_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.caja_chica_gasto_gasto_seq OWNED BY aplicacion.caja_chica_gastos.gasto;


--
-- Name: caja_chica_ingresos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.caja_chica_ingresos (
    ingreso integer NOT NULL,
    fecha date,
    descripcion character varying,
    cat_ingreso integer,
    importe bigint,
    vehiculo integer
);


ALTER TABLE aplicacion.caja_chica_ingresos OWNER TO postgres;

--
-- Name: caja_chica_ingresos_ingreso_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.caja_chica_ingresos_ingreso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.caja_chica_ingresos_ingreso_seq OWNER TO postgres;

--
-- Name: caja_chica_ingresos_ingreso_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.caja_chica_ingresos_ingreso_seq OWNED BY aplicacion.caja_chica_ingresos.ingreso;


--
-- Name: caja_gastos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.caja_gastos (
    gasto integer NOT NULL,
    fecha date,
    descripcion character varying,
    factura_numero character varying,
    cat_gasto integer,
    importe bigint
);


ALTER TABLE aplicacion.caja_gastos OWNER TO postgres;

--
-- Name: caja_gastos_gasto_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.caja_gastos_gasto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.caja_gastos_gasto_seq OWNER TO postgres;

--
-- Name: caja_gastos_gasto_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.caja_gastos_gasto_seq OWNED BY aplicacion.caja_gastos.gasto;


--
-- Name: caja_ingresos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.caja_ingresos (
    ingreso integer NOT NULL,
    fecha date,
    descripcion character varying,
    cat_ingreso integer,
    importe bigint,
    vehiculo integer
);


ALTER TABLE aplicacion.caja_ingresos OWNER TO postgres;

--
-- Name: caja_ingresos_ingreso_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.caja_ingresos_ingreso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.caja_ingresos_ingreso_seq OWNER TO postgres;

--
-- Name: caja_ingresos_ingreso_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.caja_ingresos_ingreso_seq OWNED BY aplicacion.caja_ingresos.ingreso;


--
-- Name: categorias_gastos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.categorias_gastos (
    cat_gasto integer NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.categorias_gastos OWNER TO postgres;

--
-- Name: categorias_gastos_cat_gasto_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.categorias_gastos_cat_gasto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.categorias_gastos_cat_gasto_seq OWNER TO postgres;

--
-- Name: categorias_gastos_cat_gasto_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.categorias_gastos_cat_gasto_seq OWNED BY aplicacion.categorias_gastos.cat_gasto;


--
-- Name: categorias_ingresos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.categorias_ingresos (
    cat_ingreso integer NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.categorias_ingresos OWNER TO postgres;

--
-- Name: categorias_ingresos_cat_ingreso_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.categorias_ingresos_cat_ingreso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.categorias_ingresos_cat_ingreso_seq OWNER TO postgres;

--
-- Name: categorias_ingresos_cat_ingreso_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.categorias_ingresos_cat_ingreso_seq OWNED BY aplicacion.categorias_ingresos.cat_ingreso;


--
-- Name: clientes; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.clientes (
    cliente integer NOT NULL,
    nombre character varying,
    telefono character varying,
    iden_doc character(1),
    iden_numero character varying NOT NULL,
    iden_digito character(1) DEFAULT 0,
    direccion character varying
);


ALTER TABLE aplicacion.clientes OWNER TO postgres;

--
-- Name: clientes_cliente_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.clientes_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.clientes_cliente_seq OWNER TO postgres;

--
-- Name: clientes_cliente_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.clientes_cliente_seq OWNED BY aplicacion.clientes.cliente;


--
-- Name: identificador_documentos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.identificador_documentos (
    iden_doc character(1) NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.identificador_documentos OWNER TO postgres;

--
-- Name: subcat_gastos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.subcat_gastos (
    subcat integer NOT NULL,
    categoria integer,
    descripcion character varying,
    comando character varying
);


ALTER TABLE aplicacion.subcat_gastos OWNER TO postgres;

--
-- Name: subcat_gastos_subcat_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.subcat_gastos_subcat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.subcat_gastos_subcat_seq OWNER TO postgres;

--
-- Name: subcat_gastos_subcat_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.subcat_gastos_subcat_seq OWNED BY aplicacion.subcat_gastos.subcat;


--
-- Name: transito_cargas; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.transito_cargas (
    transito_carga integer NOT NULL,
    cliente integer NOT NULL,
    vehiculo integer NOT NULL,
    buque_nombre character varying,
    fecha_transacion date,
    fecha_salida date,
    fecha_llegada_iqq date,
    fecha_llegada_py date,
    entregado boolean DEFAULT false NOT NULL,
    fecha_entrega date
);


ALTER TABLE aplicacion.transito_cargas OWNER TO postgres;

--
-- Name: transito_cargas_transito_carga_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.transito_cargas_transito_carga_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.transito_cargas_transito_carga_seq OWNER TO postgres;

--
-- Name: transito_cargas_transito_carga_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.transito_cargas_transito_carga_seq OWNED BY aplicacion.transito_cargas.transito_carga;


--
-- Name: transito_costos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.transito_costos (
    transito_costo integer NOT NULL,
    costo_origen bigint DEFAULT 0,
    flete bigint DEFAULT 0,
    despacho bigint DEFAULT 0,
    chapa bigint DEFAULT 0,
    comision_vendedor bigint DEFAULT 0,
    precio_venta bigint DEFAULT 0,
    monto_entrega bigint DEFAULT 0,
    diferencia_guardar bigint DEFAULT 0,
    nos_debe bigint DEFAULT 0,
    pago bigint DEFAULT 0,
    costo_total bigint DEFAULT 0,
    pago_venta_final bigint DEFAULT 0,
    comision_asesor bigint DEFAULT 0,
    utilidad_bruta bigint DEFAULT 0,
    proporcional_gasto bigint DEFAULT 0,
    utilidad_neta bigint DEFAULT 0,
    transito_carga integer,
    chapista bigint DEFAULT 0,
    mecanico bigint DEFAULT 0,
    accesorios_adicionales bigint DEFAULT 0,
    garantias_pagadas bigint DEFAULT 0,
    electricista bigint DEFAULT 0
);


ALTER TABLE aplicacion.transito_costos OWNER TO postgres;

--
-- Name: transito_costos_transito_costo_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.transito_costos_transito_costo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.transito_costos_transito_costo_seq OWNER TO postgres;

--
-- Name: transito_costos_transito_costo_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.transito_costos_transito_costo_seq OWNED BY aplicacion.transito_costos.transito_costo;


--
-- Name: vehiculos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.vehiculos (
    vehiculo integer NOT NULL,
    modelo character varying,
    fabricacion_agno integer,
    marca integer DEFAULT 0,
    chasis_numero character varying,
    color character varying,
    carga integer DEFAULT 0
);


ALTER TABLE aplicacion.vehiculos OWNER TO postgres;

--
-- Name: vehiculos_gastos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.vehiculos_gastos (
    id integer NOT NULL,
    vehiculo integer,
    concepto character varying,
    monto bigint,
    fecha date
);


ALTER TABLE aplicacion.vehiculos_gastos OWNER TO postgres;

--
-- Name: vehiculos_gastos_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.vehiculos_gastos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.vehiculos_gastos_id_seq OWNER TO postgres;

--
-- Name: vehiculos_gastos_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.vehiculos_gastos_id_seq OWNED BY aplicacion.vehiculos_gastos.id;


--
-- Name: vehiculos_marcas; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.vehiculos_marcas (
    marca integer NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.vehiculos_marcas OWNER TO postgres;

--
-- Name: vehiculos_marcas_marca_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.vehiculos_marcas_marca_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.vehiculos_marcas_marca_seq OWNER TO postgres;

--
-- Name: vehiculos_marcas_marca_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.vehiculos_marcas_marca_seq OWNED BY aplicacion.vehiculos_marcas.marca;


--
-- Name: vehiculos_vehiculo_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.vehiculos_vehiculo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.vehiculos_vehiculo_seq OWNER TO postgres;

--
-- Name: vehiculos_vehiculo_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.vehiculos_vehiculo_seq OWNED BY aplicacion.vehiculos.vehiculo;


--
-- Name: valmonto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.valmonto (
    chapa bigint
);


ALTER TABLE public.valmonto OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles (
    rol integer NOT NULL,
    nombre_rol character varying(140)
);


ALTER TABLE sistema.roles OWNER TO postgres;

--
-- Name: roles_rol_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_rol_seq OWNER TO postgres;

--
-- Name: roles_rol_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_rol_seq OWNED BY sistema.roles.rol;


--
-- Name: roles_x_selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles_x_selectores (
    id integer NOT NULL,
    rol integer,
    selector integer
);


ALTER TABLE sistema.roles_x_selectores OWNER TO postgres;

--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_x_selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_x_selectores_id_seq OWNER TO postgres;

--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_x_selectores_id_seq OWNED BY sistema.roles_x_selectores.id;


--
-- Name: selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores (
    id integer NOT NULL,
    superior integer,
    descripcion character varying,
    ord integer,
    link character varying
);


ALTER TABLE sistema.selectores OWNER TO postgres;

--
-- Name: selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_id_seq OWNER TO postgres;

--
-- Name: selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_id_seq OWNED BY sistema.selectores.id;


--
-- Name: selectores_x_webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores_x_webservice (
    id integer NOT NULL,
    selector integer,
    wservice integer
);


ALTER TABLE sistema.selectores_x_webservice OWNER TO postgres;

--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_x_webservice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_x_webservice_id_seq OWNER TO postgres;

--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_x_webservice_id_seq OWNED BY sistema.selectores_x_webservice.id;


--
-- Name: usuarios; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios (
    usuario integer NOT NULL,
    cuenta character varying(100),
    clave character varying(150),
    token_iat character varying
);


ALTER TABLE sistema.usuarios OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_usuario_seq OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_usuario_seq OWNED BY sistema.usuarios.usuario;


--
-- Name: usuarios_x_roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios_x_roles (
    id integer NOT NULL,
    usuario integer,
    rol integer
);


ALTER TABLE sistema.usuarios_x_roles OWNER TO postgres;

--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_x_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_x_roles_id_seq OWNER TO postgres;

--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_x_roles_id_seq OWNED BY sistema.usuarios_x_roles.id;


--
-- Name: webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.webservice (
    wservice integer NOT NULL,
    path character varying,
    nombre character varying
);


ALTER TABLE sistema.webservice OWNER TO postgres;

--
-- Name: webservice_wservice_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.webservice_wservice_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.webservice_wservice_seq OWNER TO postgres;

--
-- Name: webservice_wservice_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.webservice_wservice_seq OWNED BY sistema.webservice.wservice;


--
-- Name: gasto; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_chica_gastos ALTER COLUMN gasto SET DEFAULT nextval('aplicacion.caja_chica_gasto_gasto_seq'::regclass);


--
-- Name: ingreso; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_chica_ingresos ALTER COLUMN ingreso SET DEFAULT nextval('aplicacion.caja_chica_ingresos_ingreso_seq'::regclass);


--
-- Name: gasto; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_gastos ALTER COLUMN gasto SET DEFAULT nextval('aplicacion.caja_gastos_gasto_seq'::regclass);


--
-- Name: ingreso; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_ingresos ALTER COLUMN ingreso SET DEFAULT nextval('aplicacion.caja_ingresos_ingreso_seq'::regclass);


--
-- Name: cat_gasto; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.categorias_gastos ALTER COLUMN cat_gasto SET DEFAULT nextval('aplicacion.categorias_gastos_cat_gasto_seq'::regclass);


--
-- Name: cat_ingreso; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.categorias_ingresos ALTER COLUMN cat_ingreso SET DEFAULT nextval('aplicacion.categorias_ingresos_cat_ingreso_seq'::regclass);


--
-- Name: cliente; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.clientes ALTER COLUMN cliente SET DEFAULT nextval('aplicacion.clientes_cliente_seq'::regclass);


--
-- Name: subcat; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.subcat_gastos ALTER COLUMN subcat SET DEFAULT nextval('aplicacion.subcat_gastos_subcat_seq'::regclass);


--
-- Name: transito_carga; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.transito_cargas ALTER COLUMN transito_carga SET DEFAULT nextval('aplicacion.transito_cargas_transito_carga_seq'::regclass);


--
-- Name: transito_costo; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.transito_costos ALTER COLUMN transito_costo SET DEFAULT nextval('aplicacion.transito_costos_transito_costo_seq'::regclass);


--
-- Name: vehiculo; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.vehiculos ALTER COLUMN vehiculo SET DEFAULT nextval('aplicacion.vehiculos_vehiculo_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.vehiculos_gastos ALTER COLUMN id SET DEFAULT nextval('aplicacion.vehiculos_gastos_id_seq'::regclass);


--
-- Name: marca; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.vehiculos_marcas ALTER COLUMN marca SET DEFAULT nextval('aplicacion.vehiculos_marcas_marca_seq'::regclass);


--
-- Name: rol; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles ALTER COLUMN rol SET DEFAULT nextval('sistema.roles_rol_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores ALTER COLUMN id SET DEFAULT nextval('sistema.roles_x_selectores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_x_webservice_id_seq'::regclass);


--
-- Name: usuario; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios ALTER COLUMN usuario SET DEFAULT nextval('sistema.usuarios_usuario_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles ALTER COLUMN id SET DEFAULT nextval('sistema.usuarios_x_roles_id_seq'::regclass);


--
-- Name: wservice; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice ALTER COLUMN wservice SET DEFAULT nextval('sistema.webservice_wservice_seq'::regclass);


--
-- Name: caja_chica_gasto_gasto_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.caja_chica_gasto_gasto_seq', 334, true);


--
-- Data for Name: caja_chica_gastos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.caja_chica_gastos (gasto, fecha, descripcion, factura_numero, cat_gasto, importe, vehiculo, subcat_gasto) FROM stdin;
208	2020-02-04	CAMIONETA DE LUIS ROMAN		4	76600000	0	1
209	2020-02-04	COMPRA DE FOCOS LED PARA OFICINA		5	44000	0	1
210	2020-02-05	PAGO DE COSTOS FIJOS DE OFICINA		5	1060000	0	1
211	2020-02-05	PAGO DE FACEBOOK		7	250000	0	1
212	2020-02-05	SE DEJO EN CASA Y DESAYUNO Y FRUTAS DE LUIS		10	130000	0	1
213	2020-02-05	REPARACION DE LLANTA DE LA SANTA FE 2016		9	100000	12	1
214	2020-02-05	PAGO DE FACTURA VI PAPEL		5	165000	0	1
215	2020-02-05	GRANJA MI RETIRO PARA LA SEÑORA MONICA		35	432000	0	1
217	2020-02-06	GASTOS DE LUIS		10	65000	0	1
218	2020-01-06	PAGO DE CHAPA PARA SANTA FE 2016		37	1000000	0	1
220	2020-02-06	GASTOS DE LUIS		10	66000	0	1
221	2020-02-06	ALMUERZO DE LUIS		10	70000	0	1
222	2020-02-06	COMPRA DE ARENA		10	24000	0	1
223	2020-02-06	PAGO DE VERIFICACION DEL CLIENTE PILAR		9	30000	0	1
224	2020-02-06	CARGA DE COMBUSTIBLE		20	50000	0	1
225	2020-02-06	COMPRA DE BEBEDERO		5	650000	0	1
226	2020-02-07	DESAYUNO		10	12000	0	1
227	2020-02-07	PAGO DE ESCRIBANIA		31	100000	0	1
228	2020-02-07	PAGO PARA ASADO		10	40000	0	1
229	2020-02-07	GASTOS DE PELUQUERIA  		10	200000	0	1
230	2020-02-07	GASTOS DE LUIS		10	42000	0	1
231	2020-02-07	DESARROLLO DE SOFWARE		21	150000	0	1
232	2020-02-07	GIRO POR LA REXTON SPORTAGE		4	40430000	0	1
233	2020-02-07	PAGO DE FACEBOOK		7	781000	0	1
234	2020-02-07	PAGO DE FACEBOOK		7	655000	0	1
235	2020-02-08	GASTOS DE LUIS Y COMBUSTIBLE		10	305000	0	1
236	2020-02-08	COIMA PARA LA VERIFICACION RAPIDA DE L SANTA FE 2016		9	30000	0	1
237	2020-02-08	COMBUSTIBLE 		20	74000	0	1
238	2020-02-08	PAGO A COREA POR LA SANTA FE		26	1000000	0	1
239	2020-02-08	PAGO A OSVALDO PULIDOR		25	100000	0	1
240	2020-02-08	COMBUSTIBLE PARA TUCSON 2006		20	50000	0	1
241	2020-02-08	PAGO A NIÑERA		10	70000	0	1
242	2020-02-08	UBER		10	40000	0	1
243	2020-02-08	COMBUSTIBLE PARA SANTA FE 2016 VIAJE A PJ		20	215037	0	1
244	2020-02-08	GASTOS DE SUPER DE LUIS		10	347808	0	1
245	2020-02-09	ALMUERZO DE IDA A VIAJE DE PEDRO JUAN		28	58000	0	1
246	2020-02-09	PAGO DE FACEBOOK		7	300000	0	1
247	2020-02-10	PAGO DE ALBAÑIL		10	1000000	0	1
249	2020-02-10	ESCRIBANIA PAGO DE CHAPA DE ALFREDO		37	500000	0	1
250	2020-02-10	PAGO DE CHAPA DE ALFREDO		37	1300000	0	1
251	2020-02-10	PAGO DE TARJETA DE LUIS PERSONAL		10	48000	0	1
252	2020-02-10	ALINEACION DE LA SANTA FE 2016		9	105000	0	1
253	2020-02-10	PAGO DE AIRE DEL NADA		10	250000	0	1
254	2020-02-10	LUCES PARA CASA DE LUIS		10	476000	0	1
255	2020-02-10	COMBUSTIBLE PARA EL AUTO NADIA		10	100000	0	1
256	2020-02-11	PAGO DEL PVC DE LA CASA		10	1400000	0	1
257	2020-02-11	PAGO DE CONTADORA		5	330000	0	1
258	2020-02-11	AGUA BES		5	52000	0	1
259	2020-02-11	ALMUERZO DE TIAGO Y NADIA		10	30000	0	1
260	2020-02-11	ADELANTO DE BRIGIDA		12	50000	0	1
261	2020-02-11	COMBUSTIBLE PARA SANTA FE 2016		20	170122	0	1
262	2020-02-11	ALMUERZO EN PEDRO JUAN		28	20000	0	1
263	2020-02-11	PASAJE DE PEDRO JUAN		28	100000	0	1
264	2020-02-11	CHIPA COMPRA		10	15000	0	1
265	2020-02-11	PARA UBER DE CARDOZO POR QUE SE USO SU AUTO		10	30000	0	1
266	2020-02-11	CENA DE LOS CHICOS		10	15000	0	1
267	2020-02-11	COMBUSTIBLE PARA AUTO DE CARDOZO P/ IR A CASA		10	50000	0	1
268	2020-02-11	PAGO DE ALBAÑIL POR PVC		10	400000	0	1
269	2020-02-11	REMEDIO PARA TIAGO		10	60000	0	1
270	2020-02-12	DESAYUNO DE LUIS		10	20000	0	1
271	2020-02-12	COMBUSTIBLE DE AMBULANCIA		20	50000	0	1
272	2020-02-12	INGRESO A CAJA		5	1400000	0	1
274	2020-02-12	IMPRESION DE PAPEL ILUSTRACION		5	50000	0	1
275	2020-02-12	COMBUSTIBLE PARA TUCSON		20	50000	0	1
219	2020-02-06	COMBUSTIBLE PARA SANTA FE NEGRA		10	100000	0	\N
273	2020-02-05	CHPAS  DEFINITIVAS PARA SANTA FE 2016		37	1000000	0	1
276	2020-02-13	giro 30%		3	19400000	37	1
277	2020-02-17	COBRO DE DESPACHO AL SR. HERMINIO IBARRA		3	15432840	14	14
278	2020-02-17	COBRO DE DESPACHO AL SR. EDUARDO GABINO		3	21229120	17	14
279	2020-02-17	COBRO DE DESPACHO AL SR. ARTURO SEBASTIAN		3	19155760	18	14
280	2020-02-17	COBRO DE DESPACHO AL SR. VICTOR MANUEL		3	13626800	19	14
281	2020-02-17	COBRO PIR DESPACHO AL SR. ADRIAN JUNIOR FERNANDEZ		3	13626800	20	14
282	2020-02-17	COBRO POR DESPACHO AL SR. ALEJANDRO OSMAR 		3	18229920	21	14
283	2020-02-17	COBRO POR DESPACHO AL SR. HUGO LEANDRO		3	26210400	16	14
284	2020-02-12	COMBUSTIBLE PARA AMBULANCIA		20	50000	0	1
285	2020-02-17	CARGA DE TARJETA INFONET		32	300000	0	1
286	2020-02-12	ILUSTRACION DE PPEL		5	50000	0	1
287	2020-02-13	COMBUSTIBLE PARA TUCSON 2006		20	150000	0	1
288	2020-02-13	PAGO A GUSTAO COMPRESOR DE MIRIAN		38	650000	0	1
289	2020-02-13	DESAYUNO DE VIAJE A PILAR		28	45000	0	1
290	2020-02-13	DIESEL TUCSON 2006 SAN JUAN		20	100000	0	1
291	2020-02-14	PRODUCTO DE LIMPIEZA		5	250000	0	1
292	2020-02-14	COMPLETAR PARA PAGAR INTERNET		5	50000	0	1
293	2020-02-14	PAGO DE TERMO PARA CLIENTES		35	600000	0	1
294	2020-02-14	ADELANTO DE BRIGIDA		12	300000	0	1
295	2020-02-14	ADELANTO DE ISAAC		11	1200000	0	1
296	2020-02-14	ADELANTO DE ERWIN		39	500000	0	1
297	2020-02-14	ADELANTO DE DAVID		13	500000	0	1
298	2020-02-14	ADELANTO DE ALE		16	500000	0	1
299	2020-02-14	ADELANTO DE OSVALDO		17	500000	0	1
300	2020-02-17	PLAN DE LUIS		10	475000	0	1
304	2020-02-17	CURRIER		5	40000	0	1
301	2020-02-17	COMPRA DE SUPER		10	181000	0	1
302	2020-02-17	DISEL PODIUM		10	50000	0	1
303	2020-02-17	BATERIA PARA LA TUCSON DE ELENO		9	350000	0	1
305	2020-02-17	COPIA DE LLAVES		9	25000	0	1
306	2020-02-17	PINTURAS		10	121000	0	1
307	2020-02-17	PAGO A OSVALDO LAVADERO		25	100000	0	1
308	2020-02-17	ALMUERZO DE LUIS		10	109000	0	1
309	2020-02-17	DON FEDEICO PODADOR		10	120000	0	1
310	2020-02-17	OSVALDO INTOR		10	100000	0	1
311	2020-02-17	COMBUSTIBLE 		10	50000	0	1
312	2020-02-17	PINTOR		10	20000	0	1
313	2020-02-17	LUIS LE DIO A NADIA		10	50000	0	1
314	2020-02-17	ALMUERZO DE LUIS		10	68000	0	1
315	2020-02-17	UBER		10	12000	0	1
316	2020-02-17	UBER		10	60000	0	1
317	2020-02-17	PAGO DE FLETES		40	34020000	0	1
319	2020-02-17	COMBUSTIBLE		20	200000	0	1
320	2020-02-10	AUTO MOTOR DE LA SANTA FE 2016		9	2500000	0	1
321	2020-02-17	COMBUSTIBLE PARA CARDOZO		19	50000	0	1
216	2020-02-05	COMBUSTIBLE PARA SANTA FE 		20	100000	0	\N
322	2020-02-18	COBRO POR DESPACHO AL SEÑOR ADILIO POR LA TUCSON 2010		3	22100000	13	14
323	2020-02-18	COBRO POR DESPACHO AL SR. JOSE		3	16710760	15	14
324	2020-02-18	GIROS AL EXTERIOR		4	25044000	0	1
325	2020-02-18	COMPRA DE BATERIA PARA TUCSON 2012		3	300000	16	22
326	2020-02-18	COMBUSTIBLE PARA TUCSON 2010		3	50000	13	22
327	2020-02-18	UBER		10	30000	0	1
328	2020-02-18	LAVADERO REXTON Y TUCSON		9	120000	0	1
329	2020-02-18	TONER HP		5	375000	0	1
330	2020-02-18	ESCRIBANIA		30	150000	0	1
331	2020-02-19	PAGO DE ALQUILER  MES DE FEBRERO		5	2000000	0	1
332	2020-02-19	PAGO DE CHAPA PROVISORIA		36	150000	0	1
333	2020-02-19	GASTOS DE LUIS		10	2455000	0	1
334	2020-02-19	PAGO DE FACEBOOK		7	585000	0	1
\.


--
-- Data for Name: caja_chica_ingresos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.caja_chica_ingresos (ingreso, fecha, descripcion, cat_ingreso, importe, vehiculo) FROM stdin;
6	2020-02-03	VTA DE TUCSON 2010	5	85000000	\N
7	2020-02-04	VTA DE SPORTAGE 2008	5	20000000	\N
8	2020-02-07	VTA DE REXTON 2006	5	31000000	\N
9	2020-02-12	SALDO DEL MES PASADO	4	10644000	\N
10	2020-02-13	COBRO DE DESPACHO DE HUGO POR LA TUCSON 2012	7	26750000	\N
11	2020-02-13	VENTA DE SANTA FE 2016 COLOR GRIS OSCURO	5	100000000	\N
13	2020-02-14	COBRO DE DESPACHO DE SR.HERMINIO POR LA BONGO 2004	7	16000000	\N
14	2020-02-14	COBRO POR DESPACHO DEL SR. EDUARDO POR LA REXTON 2007	7	21580000	\N
15	2020-02-14	COBRO POR DESPACHO AL SR. ARTURO POR LA SANTA FE 2007	7	21580000	\N
16	2020-02-14	COBRO POR DESPACHO AL SR. VICTOR POR LA SPORTAGE 2008	7	16000000	\N
17	2020-02-14	COBRO POR DESPACHO AL SR. ALEJANDRO POR LA TUCSON 2008	7	19710000	\N
18	2020-02-15	COBRO POR DESPACHO AL SR. ADRIAN POR LA SPORTAGE 2005	7	16000000	\N
20	2020-02-18	vta de sportage 2008	5	25000000	\N
21	2020-02-18	COBRO POR DESPACHO AL SR. ADILIO POR LA TUCSON 2010	7	22000000	\N
22	2020-02-18	cobro por despacho al sr. jose rebey tucson 2007	7	18160000	\N
\.


--
-- Name: caja_chica_ingresos_ingreso_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.caja_chica_ingresos_ingreso_seq', 22, true);


--
-- Data for Name: caja_gastos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.caja_gastos (gasto, fecha, descripcion, factura_numero, cat_gasto, importe) FROM stdin;
\.


--
-- Name: caja_gastos_gasto_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.caja_gastos_gasto_seq', 13, true);


--
-- Data for Name: caja_ingresos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.caja_ingresos (ingreso, fecha, descripcion, cat_ingreso, importe, vehiculo) FROM stdin;
\.


--
-- Name: caja_ingresos_ingreso_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.caja_ingresos_ingreso_seq', 4, true);


--
-- Data for Name: categorias_gastos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.categorias_gastos (cat_gasto, descripcion) FROM stdin;
3	Mercaderia en Transito
4	GIROS AL EXTERIOR
5	COSTO FIJO DE OFICINA 
6	COSTO VARIABLE DE OFICINA
7	FACEBOOK
8	REGALOS PARA ASESORES 
9	RESERVA PARA SERVICE
10	SALARIO DE LUIS
11	SALARIO DE YSAAC
12	SALARIO DE BRIGIDA
13	SALARIO DE  DAVID
14	SALARIO NADIA
15	SALARIO PEDRO
16	SALARIO  ALEJANDRO 
17	SALARIO DE CARDOZO
18	SALARIO DE FABRIZIO
19	COMBUSTIBLE PARA LOGISTICA
20	COMBUSTIBLE PARA CAMIONETAS
21	CRM
22	DESPACHOS
23	ENVIO DE ENCOMIENDA
24	COMPRA DE ACCESORIO
25	PAGO A OSVALDO LAVADERO
26	CHAPISTA COREA
27	PAGS DE PRESTAMOS
28	VIAJES
29	FLETES DE IQQ
30	RECURSISTA
31	ESCRIBANIA
32	COMPRA DE CHINA/EE.UU
33	GASTOS VARIABLES 
34	ACONDICIONAMIENTO DE OFICINA
35	REGALO PARA CLIENTES
36	CHAPAS PROVISORIAS
37	CHAPAS DEFINITIVAS
38	GARANTIAS CUIERTAS
39	SALARIO DE ERWIN
40	FLETES DESDE IQQ
\.


--
-- Name: categorias_gastos_cat_gasto_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.categorias_gastos_cat_gasto_seq', 40, true);


--
-- Data for Name: categorias_ingresos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.categorias_ingresos (cat_ingreso, descripcion) FROM stdin;
5	VTA DE VEHICULOS
4	INGRESO DE CAJA
6	COBRO DE CHAPAS 
7	COBRO DE DESPACHOS
8	VTA DE CHINA /EE.UU
9	COBRO A CLIENTES CON SALDO PENDIENTE
10	COBRO DE SERVICIOS DE TALLER
\.


--
-- Name: categorias_ingresos_cat_ingreso_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.categorias_ingresos_cat_ingreso_seq', 10, true);


--
-- Data for Name: clientes; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.clientes (cliente, nombre, telefono, iden_doc, iden_numero, iden_digito, direccion) FROM stdin;
21	ELENO PINTOS QUINTANA	975122065	C	2309705	0	PILAR
22	ALFREDO AGUSTIN MARTINEZ NUÑEZ	973 684817	C	1596210	0	PEDRO JUAN CABALLERO
23	ADILIO DARIO SAMUDIO  MARTINEZ	971925804	C	3704957	0	QUIINDY
24	HERMINIO IBARRA 	981476155	C	727601	0	ITA
25	JOSE REBEY CASTILLO	971793103	C	2133810	0	ASUNCION
26	HUGO LEANDRO MEDINA BRENDANO	976414699	C	4671282	0	HERNANDARIAS
27	EDUARDO GABINO JARA RAMIREZ	981773188	C	3815142	0	ASUNCION
28	ARTURO SEBASTIAN MARECOS TRINIDAD	961566599	C	4759384	0	ASUNCION
29	VICTOR MANUEL ORREGO RAMIREZ	961 886029	C	3542010	0	LAMBARE
30	ADRIAN JUNIOR FERNANDEZ DIAZ	986 346962	C	3648773	0	LUQUE
31	ALEJANDRO OSMAR FRIAS	595 981 200285	C	8033036	0	AYOLAS
32	ROSA CAROINA HEREBIA CABRAL	973515583	C	3260412	0	CIUDAD DEL ESTE
33	GUSTAVO ALFREDO BRITEZ SALINAS 	595 976 474949	C	4444376	0	CAAGUAZU
34	CARLOS ALBERTO DUARTE GUTIERREZ		C	3536456	0	MISIONES
35	MONICA ALEJANDRA VILLALBA CABRAL	595971177755	C	2015896	0	ASUNCION
36	CIRILO RAMON RUIZ SANCHEZ		C	1225439	0	ASUNCION
37	LUIS CARLOS ZAVAN NUÑEZ	971205047	C	655823	0	ASUNCION
38	ALFREDO BLADIMIR GIMENEZ FRANCO	595994103177	C	4054093	0	BENJAMIN ACEVAL
39	AUGUSTO LEGUIZAMON ARELLANO	985775615	C	1941667	0	ITAPUA
40	ALIE FERNANDEZ DA SILVA	983532674	C	4767599	0	SANTA RITA
41	GUSTAVO JACINTO GONZALES ARRIOLA	981563183	C	3498591	0	CAPIATA
42	ANGEL ENRIQUE ROJAS FERNANDEZ		C	3218596	0	ASUNCION
43	ELISA MABRL HERRERA AQUINO		C	3287468	0	CONCEPCION
44	LUIS ALBERTO ROMAN PICO	 981 466024	C	519231	0	ASUNCION
45	JOSE DARIO AGUILAR TIRA	595973659768	C	1390349	0	CIUDAD DEL ESTE
46	MARIO ROLANDO VERA FERNANDEZ	0982764936	C	4665006	0	ÑEMBY
47	ZULEMA MONTSERRAT MEDINA SEVILA	0985449637	C	5109751	0	LAS VIOLETAS ESQ AZARES 317 VILLA ELISA
\.


--
-- Name: clientes_cliente_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.clientes_cliente_seq', 47, true);


--
-- Data for Name: identificador_documentos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.identificador_documentos (iden_doc, descripcion) FROM stdin;
R	Ruc
C	Cedula
\.


--
-- Data for Name: subcat_gastos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.subcat_gastos (subcat, categoria, descripcion, comando) FROM stdin;
1	3	Costo origen	costo_origen
2	3	Chapa	chapa
5	3	Utilidad neta	utilidad_neta
6	3	Flete	flete
7	3	Comision vendedor	comision_vendedor
8	3	Nos debe	nos_debe
12	3	Pago	pago
14	3	Despacho	despacho
15	3	Monto entrega	monto_entrega
16	3	Costo total	costo_total
3	3	Dif a guardar	diferencia_guardar
19	3	Vendedor	comision_vendedor
11	3	Precio venta	precio_venta
20	3	Chapista	chapista
21	3	Mecánico	mecanico
22	3	Accesorios adicionales	accesorios_adicionales
23	3	Garantías pagadas	garantias_pagadas
24	3	Electricista	electricista
\.


--
-- Name: subcat_gastos_subcat_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.subcat_gastos_subcat_seq', 24, true);


--
-- Data for Name: transito_cargas; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.transito_cargas (transito_carga, cliente, vehiculo, buque_nombre, fecha_transacion, fecha_salida, fecha_llegada_iqq, fecha_llegada_py, entregado, fecha_entrega) FROM stdin;
37	23	13	GLOVIS CAPTAIN	2019-11-22	2019-12-21	2020-02-04	2020-02-10	f	\N
38	24	14	GLOVIS CAPTAIN	2019-11-26	2019-12-21	2020-02-04	2020-02-10	f	\N
39	25	15	GLOVIS CAPTAIN	2019-11-26	2019-12-21	2020-02-04	2020-02-10	f	\N
40	27	17	GLOVIS CAPTAIN	2019-11-27	2019-12-21	2020-02-04	2020-02-10	f	\N
41	28	18	GLOVIS CAPTAIN	2019-11-25	2019-12-21	2020-02-04	2020-02-10	f	\N
42	29	19	GLOVIS CAPTAIN	2019-11-28	2019-12-21	2020-02-04	2020-02-10	f	\N
43	30	20	GLOVIS CAPTAIN	2019-11-27	2020-12-21	2020-02-04	2020-02-10	f	\N
45	32	22	GLOVIS CARDINAL	2019-12-06	2020-01-13	2020-02-24	2020-03-01	f	\N
46	33	23	ASIAN KING/ RCC AMERICA V.001	2019-12-12	2020-01-02	2020-02-12	2020-02-15	f	\N
47	34	24	ASIAN KING/ RCC AMERICA V.001	2019-12-18	2020-01-02	2020-02-12	2020-02-20	f	\N
48	35	25	ASIAN KING/ RCC AMERICA V.001	2019-12-21	2020-01-02	2020-02-12	2020-02-20	f	\N
44	31	21	GLOVIS CAPTAIN	2019-12-04	2019-12-21	2020-02-04	2020-02-10	f	\N
49	36	26	ASIAN KING/ RCC AMERICA V.001	2019-12-21	2020-01-02	2020-02-12	2020-02-20	f	\N
50	37	27	GLOVIS SUNLIGHT	2020-01-11	2020-01-29	2020-03-09	2020-03-20	f	\N
51	38	28	GLOVIS SUNLIGHT	2020-01-17	2020-01-29	2020-03-09	2020-03-20	f	\N
52	39	29	MORNING CHORUS	2020-01-18	2020-02-05	\N	\N	f	\N
53	40	30	MORNING CHORUS	2020-01-23	2020-02-05	\N	\N	f	\N
55	41	32	MORNING CHORUS	2020-01-25	2020-02-05	\N	\N	f	\N
56	42	33		2020-01-28	\N	\N	\N	f	\N
57	43	34	MORNING CHORUS	2019-12-03	2020-02-05	\N	\N	f	\N
58	44	35		2019-12-30	\N	\N	\N	f	\N
54	45	31	MORNING CHORUS	2020-01-24	2020-02-05	\N	\N	f	\N
59	46	36		2020-02-07	\N	\N	\N	f	\N
60	47	37		2020-02-13	2020-02-17	2020-03-17	\N	f	\N
35	21	11	GLOVIS CHALLENGE	2019-11-11	2019-11-29	2020-01-07	2020-01-18	t	\N
36	22	12	GLOVIS CHALLENGE	2019-11-29	2019-11-29	2020-01-07	2020-01-18	t	\N
61	26	16	GLOVIS CAPTAIN	2019-11-26	2019-12-21	2020-02-03	2020-02-17	f	\N
\.


--
-- Name: transito_cargas_transito_carga_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.transito_cargas_transito_carga_seq', 61, true);


--
-- Data for Name: transito_costos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.transito_costos (transito_costo, costo_origen, flete, despacho, chapa, comision_vendedor, precio_venta, monto_entrega, diferencia_guardar, nos_debe, pago, costo_total, pago_venta_final, comision_asesor, utilidad_bruta, proporcional_gasto, utilidad_neta, transito_carga, chapista, mecanico, accesorios_adicionales, garantias_pagadas, electricista) FROM stdin;
39	0	0	0	0	0	0	0	0	0	0	0	50000000	0	0	2500000	0	35	0	0	0	0	0
40	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	36	0	0	0	0	0
49	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	45	0	0	0	0	0
50	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	46	0	0	0	0	0
51	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	47	0	0	0	0	0
52	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	48	0	0	0	0	0
53	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	49	0	0	0	0	0
54	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	50	0	0	0	0	0
55	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	51	0	0	0	0	0
56	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	52	0	0	0	0	0
57	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	53	0	0	0	0	0
58	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	54	0	0	0	0	0
59	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	55	0	0	0	0	0
60	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	56	0	0	0	0	0
61	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	57	0	0	0	0	0
62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	58	0	0	0	0	0
63	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	59	0	0	0	0	0
64	19400000	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	60	0	0	0	0	0
42	0	0	15432840	0	0	0	0	0	0	0	0	0	0	0	0	0	38	0	0	0	0	0
44	0	0	21229120	0	0	0	0	0	0	0	0	0	0	0	0	0	40	0	0	0	0	0
45	0	0	19155760	0	0	0	0	0	0	0	0	0	0	0	0	0	41	0	0	0	0	0
46	0	0	13626800	0	0	0	0	0	0	0	0	0	0	0	0	0	42	0	0	0	0	0
47	0	0	13626800	0	0	0	0	0	0	0	0	0	0	0	0	0	43	0	0	0	0	0
48	0	0	18229920	0	0	0	0	0	0	0	0	0	0	0	0	0	44	0	0	0	0	0
43	0	0	16710760	0	0	0	0	0	0	0	0	0	0	0	0	0	39	0	0	0	0	0
65	0	0	26210400	0	0	0	0	0	0	0	0	0	0	0	0	0	61	0	0	300000	0	0
41	0	0	22100000	0	0	0	0	0	0	0	0	0	0	0	0	0	37	0	0	50000	0	0
\.


--
-- Name: transito_costos_transito_costo_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.transito_costos_transito_costo_seq', 65, true);


--
-- Data for Name: vehiculos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.vehiculos (vehiculo, modelo, fabricacion_agno, marca, chasis_numero, color, carga) FROM stdin;
11	TUCSON	2006	7	KMHJP81VP6U412472	PLATA	35
12	SANTA FE 	2016	7	KMHSW81UBGU597639	BLANCO	36
13	TUCSON	2010	7	kmhju81vbau058599	BLANCO	37
14	BONGO	2004	6	KNGDNM9N14K159329	AMARILLO	38
15	TUCSON	2007	7	KMHJM81VP7U584971	BLANCO	39
17	REXTON	2007	9	KPBFA2AF17P246876	BLANCO	40
18	SANTA FE	2007	7	KMHSJ81WP7U137430	NEGRO	41
19	SPORTAGE	2005	6	KNAJE55135K111540	GRIS HUMO	42
20	SPORTAGE	2005	6	KNAJE55135K013563	GRIS PLATA	43
21	TUCSON	2008	7	KMHJM81VP8U814738	BLANCO	44
22	SANTA FE	2013	7	KMHJM81VP8U814738	MARRON	45
23	SANTA FE 	2007	7	KMHSH81WP7U121017	PLATA	46
24	H1	2009	7	KMJWAH7JP9U143640	GRIS	47
25	TUCSON	2016	7	KMHJ3815GGU196686	BLANCO	48
26	SPORTAGE	2008	6	KNAJE55528K547417	PLATA	49
27	SPORTAGE 	2017	6	KNAPS81ABHA297699	AZUL	50
28	SPORTAGE	2007	6	KNAJE55537K351575	AZUL	51
29	TUCSON 	2012	7	KMHJU81VBCU361052	AZUL	52
30	SPORTAGE	2012	6	KNAPB813BCK160778	PLATA	53
31	SANTA FE 2019	2019	7	KMHS381BBKU017676 	BLANCO	54
32	SPORTAGE 	2011	6	KNAPC813BBK004666	PLATA	55
33	SANTA FE	2017	7	KMHSW81XDHU745815	BLANCO	56
34	SANTA FE 	2018	7	KMHSW81XDJU801500	GRIS HUMO	57
35	TUCSON 	2017	7	KMHJ2815GHU349953	BLANCO	58
36	REXTON	2006	9	KPBFA2AF16P221673	NEGRO	59
37	TUCSON	2013	7		BLANCO	60
16	TUCSON	2012	7	KMHJT81VBCU496116	GRIS HUMO	61
\.


--
-- Data for Name: vehiculos_gastos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.vehiculos_gastos (id, vehiculo, concepto, monto, fecha) FROM stdin;
\.


--
-- Name: vehiculos_gastos_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.vehiculos_gastos_id_seq', 1, false);


--
-- Data for Name: vehiculos_marcas; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.vehiculos_marcas (marca, descripcion) FROM stdin;
6	KIA
7	HYUNDAI
9	SSANGYONG
\.


--
-- Name: vehiculos_marcas_marca_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.vehiculos_marcas_marca_seq', 9, true);


--
-- Name: vehiculos_vehiculo_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.vehiculos_vehiculo_seq', 37, true);


--
-- Data for Name: valmonto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.valmonto (chapa) FROM stdin;
1111
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles (rol, nombre_rol) FROM stdin;
9	SysAdmin
69	auxiliar
70	Rol Secretaria
\.


--
-- Name: roles_rol_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_rol_seq', 70, true);


--
-- Data for Name: roles_x_selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles_x_selectores (id, rol, selector) FROM stdin;
3	9	36
4	9	2
5	9	3
2	9	11
25	9	53
35	9	57
42	9	62
43	9	63
44	9	64
45	9	65
46	9	67
47	9	68
54	9	69
55	9	70
56	9	72
57	9	73
58	70	57
59	70	36
60	70	53
61	70	62
62	70	63
63	70	64
64	70	70
65	70	67
66	70	68
67	70	69
68	70	72
69	70	73
\.


--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_x_selectores_id_seq', 69, true);


--
-- Data for Name: selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores (id, superior, descripcion, ord, link) FROM stdin;
2	1	Usuarios	1	/sistema/usuario/
3	1	Roles	2	/sistema/rol/
11	1	Acceso menu	3	/sistema/selector/
36	1	Salir	7	/
1	0	Sistema	10	
52	0	Administracion	11	
57	1	Cambio de password	4	/aplicacion/cambiopass/
53	52	Vehiculos	10	/aplicacion/vehiculo/
62	52	Marcas de vehiculos	30	/aplicacion/vehiculomarca/
63	52	Categoria Ingresos	40	/aplicacion/categoriaingreso/
64	52	Categoria Gastos	50	/aplicacion/categoriagasto/
65	52	Caja	60	/aplicacion/caja/
66	0	Comercial	22	
67	66	Clientes	10	/comercial/cliente/
68	66	Cargas en transito	20	/comercial/transito_carga/
69	66	Costos de transito	30	/comercial/transito_costo/
70	52	Caja Chica	70	/aplicacion/cajachica/
71	0	Consulta 	33	
73	71	Ganancia - Resumen	20	/consulta/gananciaresumen/
72	71	Ganancia -  Detalle	10	/consulta/gananciadet/
\.


--
-- Name: selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_id_seq', 73, true);


--
-- Data for Name: selectores_x_webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores_x_webservice (id, selector, wservice) FROM stdin;
\.


--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_x_webservice_id_seq', 1, false);


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios (usuario, cuenta, clave, token_iat) FROM stdin;
85	guidocar	c183b69672d1560ea126cc2c41c6be21	\N
1	root	4d186321c1a7f0f354b297e8914ab240	1582145862856
84	nadiaruiz	c70664c3681fe8979f4760f12bc8567c	1582127038291
83	secretaria	202cb962ac59075b964b07152d234b70	1580987990900
\.


--
-- Name: usuarios_usuario_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_usuario_seq', 85, true);


--
-- Data for Name: usuarios_x_roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios_x_roles (id, usuario, rol) FROM stdin;
197	1	9
199	84	9
201	83	70
200	85	69
\.


--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_x_roles_id_seq', 201, true);


--
-- Data for Name: webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.webservice (wservice, path, nombre) FROM stdin;
\.


--
-- Name: webservice_wservice_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.webservice_wservice_seq', 1, false);


--
-- Name: caja_chica_gastos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_chica_gastos
    ADD CONSTRAINT caja_chica_gastos_pkey PRIMARY KEY (gasto);


--
-- Name: caja_chica_ingresos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_chica_ingresos
    ADD CONSTRAINT caja_chica_ingresos_pkey PRIMARY KEY (ingreso);


--
-- Name: caja_gastos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_gastos
    ADD CONSTRAINT caja_gastos_pkey PRIMARY KEY (gasto);


--
-- Name: caja_ingresos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_ingresos
    ADD CONSTRAINT caja_ingresos_pkey PRIMARY KEY (ingreso);


--
-- Name: categorias_gastos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.categorias_gastos
    ADD CONSTRAINT categorias_gastos_pkey PRIMARY KEY (cat_gasto);


--
-- Name: categorias_ingresos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.categorias_ingresos
    ADD CONSTRAINT categorias_ingresos_pkey PRIMARY KEY (cat_ingreso);


--
-- Name: clientes_iden_doc_iden_numero_iden_digito_key; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.clientes
    ADD CONSTRAINT clientes_iden_doc_iden_numero_iden_digito_key UNIQUE (iden_doc, iden_numero, iden_digito);


--
-- Name: clientes_pkey1; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.clientes
    ADD CONSTRAINT clientes_pkey1 PRIMARY KEY (cliente);


--
-- Name: documento_identificador_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.identificador_documentos
    ADD CONSTRAINT documento_identificador_pkey PRIMARY KEY (iden_doc);


--
-- Name: subcat_gastos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.subcat_gastos
    ADD CONSTRAINT subcat_gastos_pkey PRIMARY KEY (subcat);


--
-- Name: transito_cargas_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.transito_cargas
    ADD CONSTRAINT transito_cargas_pkey PRIMARY KEY (transito_carga);


--
-- Name: transito_costos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.transito_costos
    ADD CONSTRAINT transito_costos_pkey PRIMARY KEY (transito_costo);


--
-- Name: vehiculos_gastos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.vehiculos_gastos
    ADD CONSTRAINT vehiculos_gastos_pkey PRIMARY KEY (id);


--
-- Name: vehiculos_marcas_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.vehiculos_marcas
    ADD CONSTRAINT vehiculos_marcas_pkey PRIMARY KEY (marca);


--
-- Name: vehiculos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.vehiculos
    ADD CONSTRAINT vehiculos_pkey PRIMARY KEY (vehiculo);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (rol);


--
-- Name: roles_x_selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_pkey PRIMARY KEY (id);


--
-- Name: selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores
    ADD CONSTRAINT selectores_pkey PRIMARY KEY (id);


--
-- Name: selectores_x_webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice
    ADD CONSTRAINT selectores_x_webservice_pkey PRIMARY KEY (id);


--
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuario);


--
-- Name: usuarios_x_roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_pkey PRIMARY KEY (id);


--
-- Name: webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice
    ADD CONSTRAINT webservice_pkey PRIMARY KEY (wservice);


--
-- Name: t_caja_chica_gastos_after_delete; Type: TRIGGER; Schema: aplicacion; Owner: postgres
--

CREATE TRIGGER t_caja_chica_gastos_after_delete AFTER DELETE ON aplicacion.caja_chica_gastos FOR EACH ROW EXECUTE PROCEDURE aplicacion.eliminar_caja_chica_gastos();


--
-- Name: t_caja_chica_gastos_after_insert; Type: TRIGGER; Schema: aplicacion; Owner: postgres
--

CREATE TRIGGER t_caja_chica_gastos_after_insert AFTER INSERT ON aplicacion.caja_chica_gastos FOR EACH ROW EXECUTE PROCEDURE aplicacion.insertar_caja_chica_gastos();


--
-- Name: t_transito_cargas_after_insert; Type: TRIGGER; Schema: aplicacion; Owner: postgres
--

CREATE TRIGGER t_transito_cargas_after_insert AFTER INSERT ON aplicacion.transito_cargas FOR EACH ROW EXECUTE PROCEDURE aplicacion.insertar_transporte_costo_registro();


--
-- Name: t_transito_cargas_after_update; Type: TRIGGER; Schema: aplicacion; Owner: postgres
--

CREATE TRIGGER t_transito_cargas_after_update AFTER UPDATE ON aplicacion.transito_cargas FOR EACH ROW EXECUTE PROCEDURE aplicacion.update_transporte_carga_vehiculo();


--
-- Name: caja_chica_gastos_cat_gasto_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_chica_gastos
    ADD CONSTRAINT caja_chica_gastos_cat_gasto_fkey FOREIGN KEY (cat_gasto) REFERENCES aplicacion.categorias_gastos(cat_gasto);


--
-- Name: caja_chica_ingresos_cat_ingreso_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_chica_ingresos
    ADD CONSTRAINT caja_chica_ingresos_cat_ingreso_fkey FOREIGN KEY (cat_ingreso) REFERENCES aplicacion.categorias_ingresos(cat_ingreso);


--
-- Name: caja_gastos_cat_gasto_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_gastos
    ADD CONSTRAINT caja_gastos_cat_gasto_fkey FOREIGN KEY (cat_gasto) REFERENCES aplicacion.categorias_gastos(cat_gasto);


--
-- Name: caja_ingresos_cat_ingreso_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.caja_ingresos
    ADD CONSTRAINT caja_ingresos_cat_ingreso_fkey FOREIGN KEY (cat_ingreso) REFERENCES aplicacion.categorias_ingresos(cat_ingreso);


--
-- Name: subcat_gastos_categoria_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.subcat_gastos
    ADD CONSTRAINT subcat_gastos_categoria_fkey FOREIGN KEY (categoria) REFERENCES aplicacion.categorias_gastos(cat_gasto);


--
-- Name: transito_cargas_cliente_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.transito_cargas
    ADD CONSTRAINT transito_cargas_cliente_fkey FOREIGN KEY (cliente) REFERENCES aplicacion.clientes(cliente);


--
-- Name: transito_cargas_vehiculo_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.transito_cargas
    ADD CONSTRAINT transito_cargas_vehiculo_fkey FOREIGN KEY (vehiculo) REFERENCES aplicacion.vehiculos(vehiculo);


--
-- Name: transito_costos_transito_carga_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.transito_costos
    ADD CONSTRAINT transito_costos_transito_carga_fkey FOREIGN KEY (transito_carga) REFERENCES aplicacion.transito_cargas(transito_carga);


--
-- Name: vehiculos_gastos_vehiculo_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.vehiculos_gastos
    ADD CONSTRAINT vehiculos_gastos_vehiculo_fkey FOREIGN KEY (vehiculo) REFERENCES aplicacion.vehiculos(vehiculo);


--
-- Name: roles_x_selectores_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- Name: roles_x_selectores_selector_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_selector_fkey FOREIGN KEY (selector) REFERENCES sistema.selectores(id);


--
-- Name: usuarios_x_roles_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- Name: usuarios_x_roles_usuario_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_usuario_fkey FOREIGN KEY (usuario) REFERENCES sistema.usuarios(usuario);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

