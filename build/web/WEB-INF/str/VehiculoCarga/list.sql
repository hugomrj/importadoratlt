


SELECT 
  vehiculos.vehiculo, 
  vehiculos.modelo, 
  vehiculos.fabricacion_agno, 
  vehiculos.chasis_numero, 
  vehiculos.color, 
  transito_cargas.entregado, 
  vehiculos_marcas.descripcion vehiculo_marca, 
  transito_cargas.cliente, 
  clientes.nombre cliente_nombre,  
  clientes.iden_numero, 
  clientes.iden_digito, 
  transito_cargas.transito_carga 


FROM 
  aplicacion.vehiculos,   aplicacion.transito_cargas, 
  aplicacion.clientes,   aplicacion.vehiculos_marcas 


WHERE 
  vehiculos.vehiculo = transito_cargas.vehiculo AND 
  clientes.cliente = transito_cargas.cliente AND 
  vehiculos_marcas.marca = vehiculos.marca  





