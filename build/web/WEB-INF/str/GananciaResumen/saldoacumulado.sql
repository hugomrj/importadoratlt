

select  gast.* ,  ingre.*,  (ingre.sum_ingreso - gast.sum_gasto)  acumulado 
 from   
( 

    SELECT 
        v1 as row,sum(caja_chica_gastos.importe  ) sum_gasto 
  
    FROM 
    aplicacion.caja_chica_gastos, aplicacion.categorias_gastos 
    WHERE 
    caja_chica_gastos.cat_gasto = categorias_gastos.cat_gasto 
    and extract(year from fecha) = v0 
    and extract(month from fecha) < v1   

  
) as gast 
FULL OUTER JOIN 
( 

    SELECT 
	v1  as row,sum(caja_chica_ingresos.importe  ) sum_ingreso  
    FROM 
    aplicacion.categorias_ingresos,   aplicacion.caja_chica_ingresos  
    WHERE  
    caja_chica_ingresos.cat_ingreso = categorias_ingresos.cat_ingreso  
    and extract(year from fecha) = v0    
    and extract(month from fecha) < v1    

  ) as ingre 
  ON gast.row = ingre.row  

