
select  gast.* , '  ' as sepa , ingre.* from 

(SELECT 
  caja_chica_gastos.gasto,   caja_chica_gastos.fecha g_fecha ,   caja_chica_gastos.descripcion g_decripcion,   caja_chica_gastos.factura_numero, 
  caja_chica_gastos.cat_gasto,   categorias_gastos.descripcion g_categoria,   caja_chica_gastos.importe g_importe,   caja_chica_gastos.vehiculo, 
  caja_chica_gastos.subcat_gasto 
,ROW_NUMBER () OVER (ORDER BY fecha) as row     
  
FROM 
  aplicacion.caja_chica_gastos, aplicacion.categorias_gastos 
WHERE 
  caja_chica_gastos.cat_gasto = categorias_gastos.cat_gasto 
  and extract(year from fecha) = v0 
  and extract(month from fecha) = v1 
) as gast 
FULL OUTER JOIN 
(SELECT 
  caja_chica_ingresos.ingreso,   caja_chica_ingresos.fecha i_fecha,   caja_chica_ingresos.descripcion i_decripcion,   caja_chica_ingresos.cat_ingreso, 
  categorias_ingresos.descripcion i_categoria,   caja_chica_ingresos.importe  i_importe,   caja_chica_ingresos.vehiculo 
  ,ROW_NUMBER () OVER (ORDER BY fecha) as row   
FROM 
  aplicacion.categorias_ingresos,   aplicacion.caja_chica_ingresos 
WHERE  
  caja_chica_ingresos.cat_ingreso = categorias_ingresos.cat_ingreso 
  and extract(year from fecha) = v0  
  and extract(month from fecha) = v1  

  ) as ingre 
  ON gast.row = ingre.row  

